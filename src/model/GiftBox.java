package model;

import java.util.ArrayList;

public class GiftBox extends Product {
	private ArrayList<Product> products = new ArrayList<>();
	private int numberOfBeers;
	
	
	/**
	 * Giftbox constructor 
	 * 
	 * tager parameter til at oprette et nyt giftbox objekt
	 * 
	 * @param name
	 * @param group
	 * @param numberOfBeers
	 * @param products
	 */
	
	public GiftBox(String name, ProductGroup group, int numberOfBeers, ArrayList<Product> products) {
		super(name, group);
		this.numberOfBeers = numberOfBeers;
		this.products = products;
	}

	/**
	 * Returns the content of a giftbox
	 */
	public String toString() {
		String s = super.toString();
	
		if(products.size() > 0) 
		for(Product p : products) {
			s += ", " + p.toString();
		}
		return s;
	}
	
	/**
	 * Method adds the product to a giftbox
	 * 
	 * @param products
	 */
	public void setProducts(ArrayList<Product> products) {
		if(!this.products.equals(products)) {
			this.products = products;
		}
	}
	
	/**
	 * Methos returns number of beers in a giftbox
	 * @return
	 */
	public int getNumberOfBeers() {
		return this.numberOfBeers;
	}
	
	/**
	 * Method returns an arraylist containing products in giftbox
	 * @return
	 */
	public ArrayList<Product> getProducts() {
		return this.products;
	}
}