package model;

import controller.Controller;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Receipt {

	private String title= new String();
	private String preamble;
	private String output = new String();
	
	/**
	 * This method is for printing the information to file
	 * as it is now it puts the date of today as the name of the file
	 * 
	 * @throws IOException if there is an error with the file being writting to
	 */
	
	public void print() throws IOException {
		FileWriter out = null;
		String outName = preamble();
		String body = body();
		title = LocalDate.now().toString().trim();
		
		try {
			out = new FileWriter(title+".txt", true);
			out.write(outName);
			out.write(body);
		}finally {
			out.close();
		}
	}
	 
	/**
	 * This method provides the heading for the receipt
	 * It takes no arguments at the moment, it just the date at the top
	 *
	 * @return String 
	 */
	
	public String preamble() {
		LocalDateTime date = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy | hh:mm");
		preamble = date.format(formatter);
		
		preamble = "\n#########################################\n"
				+"#	          Aarhus Bryghus	        #\n"
				+"#	            KVITTERING	            #\n"
				+"#         "+"Dato "+preamble+"       #\n"
				+"#########################################\n";
		return preamble;
	}
	
	public String body() {
		output = Controller.getCompletedSales().toString().substring(1, Controller.getCompletedSales().toString().length()-1).replace(", ", "");
		
		return output;
	}
}
