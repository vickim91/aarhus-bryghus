 package model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TourSale extends Sale{
	private LocalDateTime dateAndTime;
	
	public TourSale(LocalDate dateOfSale, LocalDateTime dateAndTime) {
		super(null, dateOfSale);
		this.dateAndTime = dateAndTime;
	}
	
	@Override
	public double calculateTotalPrice() {
		double price = 0;
		PriceList priceList = super.getPriceList();
		OrderLine orderLine = super.getItems().get(0);
		
		if(orderLine.getSpecialPrice() == null) {
			price = priceList.getPrice(orderLine.getProduct())*orderLine.getAmount();
			
			if(dateAndTime.toLocalTime().isAfter(LocalTime.of(16, 00))) {
				price += orderLine.getAmount() * 20;
			}
			price = price * (1-orderLine.getPercentageDiscount()/100);
		} else {
			price = orderLine.getSpecialPrice()*orderLine.getAmount();
		}
		
		return price;
	}
	
	public LocalDateTime getDateAndTime() {
		return dateAndTime;
	}
	
	@Override
	public String toString() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy - hh:mm");
		LocalDateTime date = getDateAndTime() ;
		String dateOf = date.format(formatter);
		String s = "Rundvisning" +"\nTidspunkt: " +dateOf;
		s+= super.toString().substring(11);
		if(dateAndTime.toLocalTime().isAfter(LocalTime.of(16, 00))) {
			s+= "\naftentillæg pr. person : 20kr x " + super.getItems().get(0).getAmount()+ " = " + 20*super.getItems().get(0).getAmount()+"\n" ;
		}
		return s+"\n";
	}
}
