package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import controller.Controller;
import storage.Storage;

public class TapBeerRentalTest {

	private TapBeerRental tbr, tbr2, tbr3, tbr4;
	private PriceList pricelistF;
	private Sale sale;
	private ProductGroup flaskeøl;
	private ProductGroup fadøl;
	private ProductGroup kulsyrepg;
	private Product kulsyrep;
	private Product klosterbryg;
	private Product klosterbrygfad;
	private ArrayList<OrderLine> olTapBeerList = new ArrayList<>();
	private ArrayList<OrderLine> FSitemsIn = new ArrayList<>();
	private ArrayList<OrderLine> FSitemsOut = new ArrayList<>();
	private ArrayList<OrderLine> olTapBeerList2 = new ArrayList<>();
	private ArrayList<OrderLine> olTapBeerList3 = new ArrayList<>();
	private OrderLine olFSi;
	private OrderLine olFSo;
	
	@Before
	public void setUp() throws Exception {
		pricelistF = new PriceList("NormalSalg");
		sale = new Sale(pricelistF, LocalDate.now());
		flaskeøl = new ProductGroup("Flaskeøl");
		fadøl = new ProductGroup("Fadøl");
		kulsyrepg = new ProductGroup("KulsyrePG");
		
		kulsyrep = new Product("Kulsyre", kulsyrepg);
	    klosterbryg = new  Product("Klosterbryg", flaskeøl);
		klosterbrygfad = new  Product("Klosterbryg 20L", fadøl);
		
		tbr = new TapBeerRental(pricelistF, LocalDate.of(2019, 04, 8), LocalDate.of(2019, 04, 12), LocalDate.of(2019, 04, 15), true);
		tbr2 = new TapBeerRental(pricelistF, LocalDate.of(2019, 04, 8), LocalDate.of(2019, 04, 12), LocalDate.of(2019, 04, 15), false);
		tbr3 = new TapBeerRental(pricelistF, LocalDate.of(2019, 04, 8), LocalDate.of(2019, 04, 12), LocalDate.of(2019, 04, 15), false);
		tbr4 = new TapBeerRental(pricelistF, LocalDate.of(2019, 04, 8), LocalDate.of(2019, 04, 12), LocalDate.of(2019, 04, 15), false);
		
		fadøl.setDeposit(200);
		kulsyrepg.setDeposit(1000);
		pricelistF.update(klosterbrygfad);
		pricelistF.update(kulsyrep);
		pricelistF.setPrice(klosterbrygfad, 775d);
		pricelistF.setPrice(kulsyrep, 400d);
		
		OrderLine ølol = new OrderLine(klosterbrygfad, 4, null, 0d);
		OrderLine kulol = new OrderLine(kulsyrep, 1, null, 0d);
		olFSi = new OrderLine(klosterbrygfad, 1, null, 0d);
		olFSo = new OrderLine(klosterbrygfad, 2, null, 0d);
		
		OrderLine ølol2 = new OrderLine(klosterbrygfad, 4, 500d, 0d);
		OrderLine kulol2 = new OrderLine(kulsyrep, 1, 300d, 50d);
		
		OrderLine ølol3 = new OrderLine(klosterbrygfad, 4, null, 50d);
		OrderLine kulol3 = new OrderLine(kulsyrep, 1, null, 0d);
		
	
		FSitemsIn.add(olFSi);
		FSitemsOut.add(olFSo);
		
		olTapBeerList.add(ølol);
		olTapBeerList.add(kulol);
		olTapBeerList2.add(ølol2);
		olTapBeerList2.add(kulol2);
		olTapBeerList3.add(ølol3);
		olTapBeerList3.add(kulol3);
		
		tbr.setItemsOut(olTapBeerList);
		tbr2.setItemsOut(olTapBeerList);
		tbr3.setItemsOut(olTapBeerList2);
		tbr4.setItemsOut(olTapBeerList3);
		
		tbr.setDeposit(1800);
		tbr2.setDeposit(1800);
		tbr3.setDeposit(1800);
		tbr4.setDeposit(1800);
		
		Storage.addSalesToUnpaidSales(tbr);
		Storage.addSalesToUnpaidSales(tbr2);
		Storage.addSalesToUnpaidSales(tbr3);
		Storage.addSalesToUnpaidSales(tbr4);
		
		Controller.moveSaleFromUnpaidToCompleted(tbr);
		Controller.moveSaleFromUnpaidToCompleted(tbr2);
		Controller.moveSaleFromUnpaidToCompleted(tbr3);
		Controller.moveSaleFromUnpaidToCompleted(tbr4);
		
		tbr.calculateFinalSale(new ArrayList<OrderLine>());
		tbr2.calculateFinalSale(new ArrayList<OrderLine>());
		tbr3.calculateFinalSale(new ArrayList<OrderLine>());
		tbr4.calculateFinalSale(new ArrayList<OrderLine>());
	}

	@Test
	public void testCalculateTotalPrice() {
		assertEquals(4300d, tbr.calculateTotalPrice(), 0.1d);
		assertEquals(3500d, tbr2.calculateTotalPrice(), 1d);
		assertEquals(2300d, tbr3.calculateTotalPrice(), 1d);
		assertEquals(1950d, tbr4.calculateTotalPrice(), 1d);
	}
	
	@Test
	public void testTapBeerRental() {
		TapBeerRental tbrt = new TapBeerRental(pricelistF, LocalDate.of(2019, 04, 8), LocalDate.of(2019, 04, 12), LocalDate.of(2019, 04, 15), true);
		assertNotNull(tbrt);
		assertEquals(pricelistF, tbrt.getPriceList());
		assertEquals(LocalDate.of(2019, 04, 8), tbrt.getDateOfSale());
		assertEquals(LocalDate.of(2019, 04, 12), tbrt.getDateOut());
		assertEquals(LocalDate.of(2019, 04, 15), tbrt.getDateIn());
		assertEquals(true, tbrt.isDelivery());
	}

	@Test
	public void testCalculateFinalSaleTC1() {
		//arrange & act
		ProductGroup pgFustager = new ProductGroup("Fustager");
		Product klosterbrygFustage = new Product("Klosterbryg 20l", pgFustager);
		
		OrderLine ol = new OrderLine(klosterbrygFustage, 2,null, 0d);
		ArrayList<OrderLine> itemsOut = new ArrayList();
		itemsOut.add(ol);
		
		TapBeerRental tapBeerRental = new TapBeerRental(pricelistF, LocalDate.of(2019, 1, 01), LocalDate.of(2019, 1, 06), null, false);
		tapBeerRental.setItemsOut(itemsOut);

		ArrayList<OrderLine> itemsIn = new ArrayList<OrderLine>(); 
		OrderLine ol2 = new OrderLine(klosterbrygFustage, 2,null, 0d); 
		itemsIn.add(ol2);
	
		
		//assert
		assertEquals(1, tapBeerRental.calculateFinalSale(itemsIn).size(), 0.1d);
		assertEquals(0, tapBeerRental.calculateFinalSale(itemsIn).get(0).getAmount(), 0.1d);	
	}
	
	@Test
	public void testCalculateFinalSaleTC2() {
		//arrange & act
		ProductGroup pgFustager = new ProductGroup("Fustager");
		Product klosterbrygFustage = new Product("Klosterbryg 20l", pgFustager);
		
		OrderLine ol = new OrderLine(klosterbrygFustage, 2,null, 0d);
		ArrayList<OrderLine> itemsOut = new ArrayList();
		itemsOut.add(ol);
		
		TapBeerRental tapBeerRental = new TapBeerRental(pricelistF, LocalDate.of(2019, 1, 01), LocalDate.of(2019, 1, 06), null, false);
		tapBeerRental.setItemsOut(itemsOut);

		ArrayList<OrderLine> itemsIn = new ArrayList<OrderLine>(); 
		OrderLine ol2 = new OrderLine(klosterbrygFustage, 1,null, 0d); 
		itemsIn.add(ol2);
		
		
		//assert
		assertEquals(1, tapBeerRental.calculateFinalSale(itemsIn).size(), 0.1d);
		assertEquals(1, tapBeerRental.calculateFinalSale(itemsIn).get(0).getAmount(), 0.1d);
	}
	
	@Test
	public void testCalculateFinalSaleTC3() {
		//arrange & act
		ProductGroup pgFustager = new ProductGroup("Fustager");
		Product klosterbrygFustage = new Product("Klosterbryg 20l", pgFustager);
				
		OrderLine ol = new OrderLine(klosterbrygFustage, 2,null, 0d);
		ArrayList<OrderLine> itemsOut = new ArrayList();
		itemsOut.add(ol);
				
		TapBeerRental tapBeerRental = new TapBeerRental(pricelistF, LocalDate.of(2019, 1, 01), LocalDate.of(2019, 1, 06), null, false);
		tapBeerRental.setItemsOut(itemsOut);

		ArrayList<OrderLine> itemsIn = new ArrayList<OrderLine>(); 
				
		//assert
		assertEquals(itemsOut.size(), tapBeerRental.calculateFinalSale(itemsIn).size(), 0.1d);
		assertEquals(itemsOut.get(0).getAmount(), tapBeerRental.calculateFinalSale(itemsIn).get(0).getAmount(), 0.1d);
		assertEquals(itemsOut.get(0).toString(), tapBeerRental.calculateFinalSale(itemsIn).get(0).toString());
		
	}

	@Test
	public void testGetItemsOut() {
		assertEquals(olTapBeerList, tbr.getItemsOut());
		assertEquals(olTapBeerList, tbr2.getItemsOut());
	}

	@Test
	public void testSetItemsOut() {
		ArrayList<OrderLine> temp = new ArrayList<>();
		temp.add(new OrderLine(klosterbryg, 2, null, null));
		tbr.setItemsOut(temp);
		assertEquals(temp, tbr.getItemsOut());
	}

	@Test
	public void testGetDeposit() {
		tbr.setDeposit(800);
		assertEquals(800, tbr.getDeposit(), 1d);
	}

	@Test
	public void testSetDeposit() {
		tbr.setDeposit(800);
		assertEquals(800, tbr.getDeposit(), 1d);
	}

	@Test
	public void testGetDateOut() {
		assertEquals(LocalDate.of(2019, 04, 12), tbr.getDateOut());
	}

	@Test
	public void testSetDateOut() {
		tbr.setDateIn(LocalDate.of(2019, 04, 12));
		assertEquals(LocalDate.of(2019, 04, 12), tbr.getDateOut());
	}

	@Test
	public void testGetDateIn() {
		assertEquals(LocalDate.of(2019, 04, 15), tbr.getDateIn());
	}

	@Test
	public void testSetDateIn() {
		tbr.setDateIn(LocalDate.of(2019, 04, 15));
		assertEquals(LocalDate.of(2019, 04, 15), tbr.getDateIn());
	}

	@Test
	public void testIsDelivery() {
		TapBeerRental tempTBR = new TapBeerRental(pricelistF, LocalDate.of(2019, 04, 8), LocalDate.of(2019, 04, 12), LocalDate.of(2019, 04, 15), false);
		assertEquals(false, tempTBR.isDelivery());
	}
	
	@Test
	public void testIsDelivery2() {
		TapBeerRental tempTBR2 = new TapBeerRental(pricelistF, LocalDate.of(2019, 04, 8), LocalDate.of(2019, 04, 12), LocalDate.of(2019, 04, 15), true);
		assertEquals(true, tempTBR2.isDelivery());
	}

	@Test
	public void testGetDeliveryFee() {
		assertEquals(800d, tbr.getActualDeliveryFee(), 1d);
	}

	@Test
	public void testGetActualDeliveryFee() {
		assertEquals(800, tbr.getActualDeliveryFee(), 1d);
	}

	@Test
	public void testCalculateDifference() {
		assertEquals(1700, tbr2.calculateDifference(), 1d);
		assertEquals(2500, tbr.calculateDifference(), 1d);
		assertEquals(500, tbr3.calculateDifference(), 1d);
	}

}
