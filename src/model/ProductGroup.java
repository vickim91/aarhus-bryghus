package model;

import java.util.ArrayList;

public class ProductGroup {
	private String name;
	private ArrayList<Product> products = new ArrayList<>();
	private double deposit =0;
	private boolean isRental = false;

	public ProductGroup(String name) {
		this.name = name;
	}

	public Product createProduct(String name) {
		Product product = new Product(name, this);
		if(!products.contains(product)) {
			products.add(product);
		}
		return product;
	}
	
	public GiftBox createGiftBox(String name, ProductGroup group, int numberOfBeers, ArrayList<Product> products) {
		GiftBox giftBox = new GiftBox(name, group, numberOfBeers, products);
		this.products.add(giftBox);
		
		return giftBox;
	}

	public ArrayList<Product> getProducts() {
		return products;
	}

	public String toString() {
		return this.name;
	}
	
	public void removeProduct(Product product) {
		products.remove(product);
	}

	public void setProducts(ArrayList<Product> products) {
		this.products = products;
	}

	public double getDeposit() {
		return deposit; 
	}

	public void setDeposit(double deposit) {
		this.deposit = deposit;
	}
	
	public boolean isRental() {
		return isRental;
	}
	
	public void setIsRental(boolean isRental) {
		this.isRental = isRental;
	}	
}
