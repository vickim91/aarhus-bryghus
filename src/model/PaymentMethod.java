package model;

public enum PaymentMethod {
	CREDITCARD, CASH, MOBILEPAY, INVOICE, BEERCARD;
}
