package model;
	
import java.time.LocalDate;
import java.util.ArrayList;
	
public class TapBeerRental  extends Sale{
		
	private ArrayList<OrderLine> itemsOut = new ArrayList();
	private double deposit;
	private LocalDate dateOut;
	private LocalDate dateIn;
	private boolean delivery;
	private double deliveryFee = 800;
	
	public TapBeerRental(PriceList priceList, LocalDate dateOfSale, LocalDate dateOut, LocalDate dateIn, boolean delivery ) {
		super(priceList, dateOfSale);
		this.dateOut = dateOut;
		this.dateIn = dateIn;
		this.delivery = delivery;
	}
		
	public ArrayList<OrderLine> calculateFinalSale(ArrayList<OrderLine> itemsIn) {
		ArrayList<OrderLine> tempList = new ArrayList();
		
		for(OrderLine ol : itemsOut) {
			OrderLine olTemp = new OrderLine(ol.getProduct(), ol.getAmount(), ol.getSpecialPrice(), ol.getPercentageDiscount());
			tempList.add(olTemp);
		}
		
		for (OrderLine ol : tempList) {
			for (OrderLine olIn : itemsIn) {
				if(olIn.getProduct().equals(ol.getProduct())) {
					ol.setAmount(ol.getAmount()-olIn.getAmount());
				}
			}
		}
		super.setItems(tempList);
		
		return tempList;
	}
	
	public ArrayList<OrderLine> getItemsOut() {
		return itemsOut;
	}
	
	public void setItemsOut(ArrayList<OrderLine> itemsOut) {
		this.itemsOut = itemsOut;
	}
	
	public double getDeposit() {
		return deposit;
	}
	
	public void setDeposit(double deposit) {
		this.deposit = deposit;
	}
	
	public LocalDate getDateOut() {
		return dateOut;
	}
	
	public void setDateOut(LocalDate dateOut) {
		this.dateOut = dateOut;
	}
	
	public LocalDate getDateIn() {
		return dateIn;
	}
	
	public void setDateIn(LocalDate dateIn) {
		this.dateIn = dateIn;
	}
	
	public boolean isDelivery() {
		return delivery;
	}	
	
	public double getActualDeliveryFee() { 
		if(isDelivery()) {
			return deliveryFee;
		} else {
			return 0;
		}
	}
		
	public double calculateDifference() {
		double sum = super.calculateTotalPrice();
		if(isDelivery())
			sum += deliveryFee;
			
		sum -= deposit;
		return sum;
	}
	
	@Override
	public double calculateTotalPrice() {
		double sum = super.calculateTotalPrice();
		if(isDelivery())
			sum += getActualDeliveryFee();
		
		return sum;
	}
		
    @Override public String toString() {
    	String s = "Fadølsudlejning";
    	if(isDelivery()) {
    		s += "\nleveringsgebyr: " + deliveryFee;
    	}
    	
    	if(super.getPaymentMethod() == null)
    		s+= "\n betalt pant: " + getDeposit();
    	
    	s += super.toString().substring(11);
    	
    	return s+"\n";
    }
}
