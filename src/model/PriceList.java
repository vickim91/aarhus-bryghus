package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import controller.Pre;

public class PriceList implements Observer {
	
	private String name;
	private Map<Product, Double> priceMap = new HashMap<>();

	public PriceList(String name) {
		this.name = name;
	}

	public void setPrice(Product product, Double price) {
		if(!(product instanceof GiftBox)) {
			Pre.require(priceMap.containsKey(product));
		}
		priceMap.put(product, price);
	}
	
	
	public void setPriceForGroup(ProductGroup productGroup, Double price){
		Pre.require(productGroup != null);
		for(Product p : productGroup.getProducts()) {
			if (priceMap.containsKey(p)) {
				priceMap.put(p, price);
			}
		}		
	}	

	public void updateProducts(ArrayList<ProductGroup> groups) {
		for (ProductGroup pg : groups) {
			for (Product p : pg.getProducts()) {
				if (!priceMap.containsKey(p)) {
					priceMap.put(p, null);
				}
			}
		}
	}

	public Double getPrice(Product product) {
		Double d = 0d;
		if (priceMap.containsKey(product)) {
			return priceMap.get(product);
		} else
			return d;
	}
	
	public Map<Product, Double> getPriceMap() {
		return priceMap;
	}

	public void setPriceMap(Map<Product, Double> priceMap) {
		this.priceMap = priceMap;
	}

	public String toString() {
		return this.name;
	}

	@Override
	public void update(Product product) {
		priceMap.put(product, null);
	}
}
