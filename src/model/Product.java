package model;

import controller.Pre;

public class Product  {
	private String name;
	private ProductGroup group;
	
	public Product(String name, ProductGroup group) {
		Pre.require(name != null);
		Pre.require(group != null);
		this.name = name;
		this.group = group;
	}
	
	public String toString() {
		return name;
	}

	public String getName() {
		return name;
	}

	public ProductGroup getGroup() {
		return group;
	}	
}
