package model;

import controller.Pre;

public class OrderLine {

	private Product product;
	private int amount;
	private Double specialPrice;
	private Double percentageDiscount;
	
	/**
	 * Denne metode er constructor for orderline klassen
	 * den tildeler de givne parameter til tilsvarende felter
	 * 
	 * Pre: product != null 
	 * Pre: amount >= 2
	 * 
	 * @param product
	 * @param amount
	 * @param specialPrice
	 * @param percentageDiscount
	 */
	
	public OrderLine(Product product, int amount, Double specialPrice, Double percentageDiscount) {
		Pre.require(product != null);
		Pre.require(amount > 0);
		this.product = product;
		this.amount = amount;
		this.specialPrice = specialPrice;
		this.percentageDiscount = percentageDiscount;

	}

	public Product getProduct() {
		return product;
	}

	public int getAmount() {
		return amount;
	}

	public Double getSpecialPrice() {
		return specialPrice;
	}

	public Double getPercentageDiscount() {
		return percentageDiscount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String toString() {
		return "[" + product.toString() + "] x " + amount;
	}	
}
