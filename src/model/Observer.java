package model;

public interface Observer {
	public void update(Product product);
}
