package model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Sale {
	private PriceList priceList;
	private ArrayList<OrderLine> orderLines = new ArrayList<>();
	private LocalDate dateOfSale;
	private PaymentMethod paymentMethod;

	public Sale(PriceList priceList, LocalDate dateOfSale) {
		this.priceList = priceList;
		this.dateOfSale = dateOfSale;
	}

	public PriceList getPriceList() {
		return priceList;
	}

	public void setPriceList(PriceList priceList) {
		this.priceList = priceList;
	}

	public LocalDate getDateOfSale() {
		return dateOfSale;
	}

	public void setDateOfSale(LocalDate dateOfSale) {
		this.dateOfSale = dateOfSale;
	}

	public ArrayList<OrderLine> getItems() {
		return orderLines;
	}

	public void setItems(ArrayList<OrderLine> items) {
		this.orderLines = items;
	}

	public double calculateTotalPrice() {
		double price = 0;
		for (OrderLine ol : getItems())
			if (ol.getSpecialPrice() == null)
				price += priceList.getPrice(ol.getProduct()) * ol.getAmount() * (1 - ol.getPercentageDiscount() / 100);
			else
				price += ol.getSpecialPrice() * ol.getAmount();

		return price;
	}

	public String toString() {
		String s = "normal salg";
		for (OrderLine ol : getItems()) {
			s += "\n" + ol.toString() + " = " + priceList.getPrice(ol.getProduct()) * ol.getAmount();
		}

		if (getPaymentMethod() != null)
			s += "\n" + getPriceList().toString() + " " + getPaymentMethod() + "\n samlet pris: "
					+ calculateTotalPrice() + "\n salgsdato: " + this.getDateOfSale();
		else
			s += "\n" + getPriceList().toString() + " ikke betalt" + "\n salgsdato: " + this.getDateOfSale();
		
		return s;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
}
