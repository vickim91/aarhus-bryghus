package model;

public class Payment {
	private double amount;
	private String info;
	private PaymentMethod paymentMethod;
	
	public Payment(double amount, String info, PaymentMethod paymentMethod) {
		this.amount = amount;
		this.info = info;
		this.paymentMethod = paymentMethod;
	}

	public double getAmount() {
		return amount;
	}

	public String getInfo() {
		return info;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
}
