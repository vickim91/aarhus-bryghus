package controller;

import static org.junit.Assert.*;
import java.time.LocalDate;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import model.OrderLine;
import model.PaymentMethod;
import model.PriceList;
import model.Product;
import model.ProductGroup;
import model.Sale;
import storage.Storage;

public class ControllerTest {

	private PriceList priceListFridayBar;
	private Sale sale1;
	private Sale sale2;
	private ProductGroup productGroupFlaskeøl;
	private ProductGroup productGroupFadøl;

	@Before
	public void setUp() throws Exception {
		productGroupFlaskeøl = new ProductGroup("Flaskeøl");
		productGroupFadøl = new ProductGroup("Fadøl");
		Storage.addProductGroup(productGroupFlaskeøl);
		Storage.addProductGroup(productGroupFadøl);
		Product klosterøl = productGroupFlaskeøl.createProduct("Klosterbryg");
		ArrayList<OrderLine> orderLineList1 = new ArrayList<>();
		ArrayList<OrderLine> orderLineList2 = new ArrayList<>();
		orderLineList1.add(new OrderLine(klosterøl, 4, null, 0d));
		orderLineList2.add(new OrderLine(klosterøl, 2, null, 0d));
				
		priceListFridayBar = new PriceList("Fredagsbar");

		sale1 = new Sale(priceListFridayBar, LocalDate.of(2019, 04, 10));
		sale2 = new Sale(priceListFridayBar, LocalDate.of(2019, 04, 12));
		
		sale1.setItems(orderLineList1);
		sale2.setItems(orderLineList2);
		
		sale1.setPaymentMethod(PaymentMethod.BEERCARD);
		sale2.setPaymentMethod(PaymentMethod.BEERCARD);
		
		Storage.addSaleToCompletedSales(sale1);
		Storage.addSaleToCompletedSales(sale2);
	}
	
	@After
	public void tearDown() throws Exception {
		Storage.removeSaleFromCompletedSales(sale1);
		Storage.removeSaleFromCompletedSales(sale2);
		Storage.removeProductGroup(productGroupFadøl);
		Storage.removeProductGroup(productGroupFlaskeøl);
	}
	
	@Test
	public void testGetCompletedSalesForPeriodTC1() {
		//act
		ArrayList<Sale> saleExcepted = new ArrayList<>();
		
		//assert
		assertEquals(saleExcepted, (Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 7), LocalDate.of(2019, 04, 9))));
	}
	
	@Test
	public void testGetCompletedSalesForPeriodTC2() {
		//act
		ArrayList<Sale> saleExcepted = new ArrayList<>();

		//assert
		assertEquals(saleExcepted, (Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 13), LocalDate.of(2019, 04, 15))));
	}
	
	@Test
	public void testGetCompletedSalesForPeriodTC3() {
		//act
		ArrayList<Sale> saleExcepted = new ArrayList<>();
		saleExcepted.add(sale1);
		saleExcepted.add(sale2);
		
		//assert
		assertEquals(saleExcepted, (Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 10), LocalDate.of(2019, 04, 13))));
	}
	
	@Test
	public void testGetCompletedSalesForPeriodTC4() {
		//act
		ArrayList<Sale> saleExcepted = new ArrayList<>();
		saleExcepted.add(sale1);
		saleExcepted.add(sale2);

		//assert
		assertEquals(saleExcepted, (Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 10), LocalDate.of(2019, 04, 12))));
	}
	
	@Test
	public void testGetCompletedSalesForPeriodTC5() {
		//act
		ArrayList<Sale> saleExcepted = new ArrayList<>();
		saleExcepted.add(sale1);
		
		//assert
		assertEquals(saleExcepted, (Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 10), LocalDate.of(2019, 04, 11))));
	}
	
	@Test
	public void testGetCompletedSalesForPeriodTC6() {
		//act
		ArrayList<Sale> saleExcepted = new ArrayList<>();
		saleExcepted.add(sale1);
		
		//assert
		assertEquals(saleExcepted, (Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 10), LocalDate.of(2019, 04, 10))));
	}
	
	@Test
	public void testGetCompletedSalesForPeriodTC7() {
		//act
		ArrayList<Sale> saleExcepted = new ArrayList<>();
		saleExcepted.add(sale2);
		
		//assert
		assertEquals(saleExcepted, (Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 11), LocalDate.of(2019, 04, 12))));
	}
	
	@Test
	public void testGetCompletedSalesForPeriodTC8() {
		//act
		ArrayList<Sale> saleExcepted = new ArrayList<>();
		
		//assert
		assertEquals(saleExcepted, (Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 11), LocalDate.of(2019, 04, 11))));
	}
	
	@Test
	public void testGetCompletedSalesForPeriodTC9() {
		//act
		ArrayList<Sale> saleExcepted = new ArrayList<>();
		saleExcepted.add(sale2);
		
		//assert
		assertEquals(saleExcepted, (Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 12), LocalDate.of(2019, 04, 14))));
	}
	
	@Test
	public void testGetCompletedSalesForPeriodTC10() {
		//act
		ArrayList<Sale> saleExcepted = new ArrayList<>();
		saleExcepted.add(sale2);
		
		//assert
		assertEquals(saleExcepted, (Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 12), LocalDate.of(2019, 04, 12))));
	}
	
	@Test
	public void testgetBeersSoldWithBeerCardTC1() {
		//assert
		assertEquals(0, Controller.getBeersSoldWithBeerCard(LocalDate.of(2019, 04, 01), LocalDate.of(2019, 04, 03)));
	}
	
	@Test
	public void testgetBeersSoldWithBeerCardTC2() {
		//assert
		assertEquals(0, Controller.getBeersSoldWithBeerCard(LocalDate.of(2019, 04, 13), LocalDate.of(2019, 04, 14)));
	}
	
	@Test
	public void testgetBeersSoldWithBeerCardTC3() {
		//assert		
		assertEquals(6, Controller.getBeersSoldWithBeerCard(LocalDate.of(2019, 04, 10), LocalDate.of(2019, 04, 12)));
	}
	
	@Test
	public void testgetBeersSoldWithBeerCardTC4() {
		//assert		
		assertEquals(4, Controller.getBeersSoldWithBeerCard(LocalDate.of(2019, 04, 10), LocalDate.of(2019, 04, 11)));
	}
	
	@Test
	public void testgetBeersSoldWithBeerCardTC5() {
		//assert		
		assertEquals(4, Controller.getBeersSoldWithBeerCard(LocalDate.of(2019, 04, 10), LocalDate.of(2019, 04, 10)));
	}
	
	@Test
	public void testgetBeersSoldWithBeerCardTC6() {
		//assert		
		assertEquals(0, Controller.getBeersSoldWithBeerCard(LocalDate.of(2019, 04, 11), LocalDate.of(2019, 04, 11)));
	}
	
	@Test
	public void testgetBeersSoldWithBeerCardTC8() {
		//assert		
		assertEquals(2, Controller.getBeersSoldWithBeerCard(LocalDate.of(2019, 04, 12), LocalDate.of(2019, 04, 12)));
	}
	
	@Test
	public void testgetCompletedSalesForPeriodTC1() {
		//act
		ArrayList<Sale> salesExpected = new ArrayList<>();
		
		//assert		
		assertEquals(salesExpected.size(), Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 01), LocalDate.of(2019, 04, 03)).size());
	}
	
	@Test
	public void testgetCompletedSalesForPeriodTC2() {
		//act
		ArrayList<Sale> salesExpected = new ArrayList<>();
		
		//assert		
		assertEquals(salesExpected.size(), Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 13), LocalDate.of(2019, 04, 14)).size());
	}
	
	@Test
	public void testgetCompletedSalesForPeriodTC3() {
		//act
		ArrayList<Sale> salesExpected = new ArrayList<>();
		salesExpected.add(sale1);
		salesExpected.add(sale2);
		
		//assert		
		assertEquals(salesExpected.size(), Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 10), LocalDate.of(2019, 04, 12)).size());
		assertTrue(Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 10), LocalDate.of(2019, 04, 12)).contains(sale1));
		assertTrue(Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 10), LocalDate.of(2019, 04, 12)).contains(sale2));
	}
	
	@Test
	public void testgetCompletedSalesForPeriodTC4() {
		//act
		ArrayList<Sale> salesExpected = new ArrayList<>();
		salesExpected.add(sale1);
		
		//assert		
		assertEquals(salesExpected.size(), Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 10), LocalDate.of(2019, 04, 11)).size());
		assertTrue(Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 10), LocalDate.of(2019, 04, 11)).contains(sale1));
	}
	
	@Test
	public void testgetCompletedSalesForPeriodTC5() {
		//act
		ArrayList<Sale> salesExpected = new ArrayList<>();
		salesExpected.add(sale1);
		
		//assert		
		assertEquals(salesExpected.size(), Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 10), LocalDate.of(2019, 04, 10)).size());
		assertTrue(Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 10), LocalDate.of(2019, 04, 10)).contains(sale1));
	}
	
	@Test
	public void testgetCompletedSalesForPeriodTC6() {
		//act
		ArrayList<Sale> salesExpected = new ArrayList<>();
		
		//assert		
		assertEquals(salesExpected.size(), Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 11), LocalDate.of(2019, 04, 11)).size());
	}
	
	@Test
	public void testgetCompletedSalesForPeriodTC7() {
		//act
		ArrayList<Sale> salesExpected = new ArrayList<>();
		salesExpected.add(sale2);
		
		//assert		
		assertEquals(salesExpected.size(), Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 11), LocalDate.of(2019, 04, 12)).size());
		assertTrue(Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 11), LocalDate.of(2019, 04, 12)).contains(sale2));
	}
	
	@Test
	public void testgetCompletedSalesForPeriodTC8() {
		//act
		ArrayList<Sale> salesExpected = new ArrayList<>();
		salesExpected.add(sale2);
		
		//assert		
		assertEquals(salesExpected.size(), Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 12), LocalDate.of(2019, 04, 12)).size());
		assertTrue(Controller.getCompletedSalesForPeriod(LocalDate.of(2019, 04, 12), LocalDate.of(2019, 04, 12)).contains(sale2));
	}
}
