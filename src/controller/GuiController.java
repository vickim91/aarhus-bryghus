package controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import model.GiftBox;
import model.OrderLine;
import model.PriceList;
import model.Product;
import model.ProductGroup;

public class GuiController {
		
	public static boolean checkIfPriceListIsValid(PriceList priceList, ArrayList<OrderLine> orderLines ) {
		
		for(OrderLine ol :  orderLines) {
			
			for (HashMap.Entry<Product, Double> entry : priceList.getPriceMap().entrySet()) {
			    Product key = entry.getKey();
			    Double value = entry.getValue();
			    
			    if (ol.getProduct().equals(key) && value == null) 			    	
			    	return false;
			}
		}
		return true;
	}
	

	public static ArrayList<Product> matchViewWithPriceList(ProductGroup productGroup, PriceList priceList) {
		
		ArrayList<Product> productsMatchedWithPriceMap = new ArrayList();
		
		for (Product p : productGroup.getProducts()) {
			for (HashMap.Entry<Product, Double> entry : priceList.getPriceMap().entrySet()) {
			    Product key = entry.getKey();
			    Double value = entry.getValue();
			    if (p.equals(key) && value != null)
			    	productsMatchedWithPriceMap.add(p);
			}
		}
		return productsMatchedWithPriceMap;
	}
	
	public static ArrayList<ProductGroup> matchProductGroupWithPriceList( PriceList priceList) {
		
		ArrayList<ProductGroup> productGroups = new ArrayList(Controller.getProductGroups());
		ListIterator<ProductGroup> iterator = productGroups.listIterator();
		
		while (iterator.hasNext()) {
			ProductGroup pg =iterator.next();
				ArrayList<Product> tempP = matchViewWithPriceList(pg, priceList);
				if(!tempP.isEmpty()) {
					pg.setProducts(tempP);
				} else {
					iterator.remove();
				}
		}
		return productGroups;
	}

	

	
	public static boolean checkIfNumbers(String s) {
		
		for (int i = 0; i < s.length(); i++) {
			if(!Character.isDigit(s.charAt(i)) && s.charAt(i) != (char) 46) {
				return false;	
			}
		}
		return true;
	}
	
	public static boolean checkifRentalDateIsValid(LocalDate dateOut, LocalDate dateIn)	{
		boolean isValid = true;
		
		if(dateOut.isAfter(dateIn))	{
			isValid = false;
		}
	
		return isValid;
	}
	
	public static void invalidInputAlert(String string) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Fejlbesked");
		alert.setHeaderText("Ugyldigt input");
		alert.setContentText(string);
		alert.showAndWait();
		throw new IllegalArgumentException("Invalid Input type");
	}
	
	public static void noValueSelectedAlert(String string) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Fejlbesked");
		alert.setHeaderText("ingen værdi angivet");
		alert.setContentText(string);
		alert.showAndWait();
		throw new IllegalArgumentException("No Value Selected");
	}
	
	public static void InvalidDateSelectedAlert(String string) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Fejlbesked");
		alert.setHeaderText("Fejl i dato");
		alert.setContentText(string);
		alert.showAndWait();
		throw new IllegalArgumentException("Invalid dates selected");
	}
	
	public static ArrayList<Product> returnGiftBoxTemplates(ProductGroup productGroup) {
		ArrayList<Product> giftBoxTemplates = new ArrayList<>();
		
		for (Product p : productGroup.getProducts()) {
			if(p instanceof GiftBox) {
				
				if (((GiftBox) p).getProducts().size() == 0) {
					System.out.println("en tilføjet til giftboxtemp");
					giftBoxTemplates.add(p);
				}
			}
		}
		return giftBoxTemplates;
	}
	
	public static ArrayList<Product> returnGiftBoxTemplatesMatchedWithPriceList(ProductGroup productGroup, PriceList priceList) {
		
		ArrayList<Product> matchedList = GuiController.matchViewWithPriceList(productGroup, priceList);
		ArrayList<Product> matchedTemplates = new ArrayList<>();

		for (Product p : matchedList) {
			if(p instanceof GiftBox) {
				
				if (((GiftBox) p).getProducts().size() == 0) {
					matchedTemplates.add(p);
				}
			}
		}
		return matchedTemplates;
	}
}
