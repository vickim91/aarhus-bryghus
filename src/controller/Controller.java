package controller;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import model.GiftBox;
import model.OrderLine;
import model.PaymentMethod;
import model.PriceList;
import model.Product;
import model.ProductGroup;
import model.Sale;
import model.TapBeerRental;
import model.TourSale;
import storage.Storage;

public class Controller {
	
	//update
	/**
	 * Metoden opdatere produkterne i prislisten
	 * 
	 * @param priceList
	 */
	public static void updatePriceList(PriceList priceList) {
		ArrayList<ProductGroup> productGroups = Storage.getProductGroups();
		priceList.updateProducts(productGroups);
	}
	
	//observer pattern
	/**
	 * Metoden tager et produkt og en prisliste og tilføjer produktet dertil
	 * 
	 * @param product
	 * @param priceList
	 */
	public static void addProductToPriceList(Product product, PriceList priceList) {
		Pre.require(product != null);
		Pre.require(priceList != null);
		priceList.update(product);
	}
	
	//create methods
	/**
	 * Metoden skaber et nyt produkt i en given produktgruppe
	 * 
	 * Pre: group != null
	 * Pre: productName.length() != 0
	 * 
	 * @param group
	 * @param productName
	 * @return product
	 */
	public static Product createProduct(ProductGroup group, String productName) {
		Pre.require(group != null);
		Pre.require(productName.length() != 0);
		
		return group.createProduct(productName);
	}
	
	/**
	 * Metoden skaber en ny produktgruppe og gemmer den i storage
	 * 
	 * Pre: name.length() > 0
	 * 
	 * @param name
	 * @return ProductGroup
	 */
	public static ProductGroup createProductGroup(String name) {
		Pre.require(name.length() != 0);
		
		ProductGroup group = new ProductGroup(name);
		Storage.addProductGroup(group);
		return group;
	}
		
	
	/**
	 * Metoden skaber en ny prisliste og gemmer den i storage
	 * 
	 * Pre: name.length() != 0
	 * 
	 * @param name
	 * @return PriceList
	 */
	public static PriceList createPriceList(String name) {
		Pre.require(name.length() != 0);
		
		PriceList list = new PriceList(name);
		Storage.addPriceList(list);
		return list;
	}
	
	/**
	 * Metoden opretter en ny ordrerlinje med givne parameter
	 * specialPrice kan nulles og percentageDiscount sætte til 0 hvis intet er sat
	 * 
	 * Pre: product != null
	 * Pre: amount > 0
	 * 
	 * @param product
	 * @param amount
	 * @param specialPrice
	 * @param percentageDiscount
	 * @return OrderLine
	 */
	public static OrderLine createOrderLine(Product product, int amount, Double specialPrice, Double percentageDiscount) {
		Pre.require(product != null);
		Pre.require(amount > 0);
	 
		OrderLine ol = new OrderLine(product, amount, specialPrice, percentageDiscount);

	 return ol;		
	}
	
	/**
	 * Metoden opretter en ny giftbox med given navn og antal øl
	 * 
	 * Pre: name.length() > 0
	 * Pre: group != null
	 * Pre: numberOfBeers > 0
	 * Pre: products != null
	 * 
	 * @param name
	 * @param group
	 * @param numberOfBeers
	 * @param products
	 * @return GiftBox
	 */
	public static GiftBox createGiftBox(String name, ProductGroup group, int numberOfBeers, ArrayList<Product> products) {
		Pre.require(name.length() > 0);
		Pre.require(group != null);
		Pre.require(products != null);
		
		GiftBox giftBox = group.createGiftBox(name, group, numberOfBeers, products);
		
		return giftBox;
	}
		
	/**
	 * Metoden opretter en ny tapBeerRental
	 * 
	 * Pre: priceList != null
	 * Pre: itemsOut.size() > 0
	 * 
	 * @param priceList
	 * @param itemsOut
	 * @param dateIn
	 * @param dateOut
	 * @param delivery
	 * @return TapBeerRental
	 */
	public static TapBeerRental createTapBeerRental(PriceList priceList, ArrayList<OrderLine> itemsOut, LocalDate dateIn, LocalDate dateOut, boolean delivery){
		Pre.require(priceList != null);
		Pre.require(itemsOut.size() > 0);
		
		LocalDate dateOfSale = LocalDate.now();
		TapBeerRental tapBeerRental = new TapBeerRental(priceList, dateOfSale, dateOut, dateIn, delivery);
		ArrayList<OrderLine> ol = new ArrayList<OrderLine>();
		int counter=0;
		for (OrderLine o : itemsOut) {
			ol.add(o);
			counter++;
		}
		tapBeerRental.setItemsOut(ol);
		tapBeerRental.setPaymentMethod(null);
		tapBeerRental.setDeposit(calculateDeposit(itemsOut));
		Storage.addSalesToUnpaidSales(tapBeerRental);
		
		return tapBeerRental;
	}
	
	
	/**
	 * Metoden opretter et normalSale, herefter sættes dato salg oprettes og afsluttes med items og paymentmethod
	 *  
	 * @param priceList
	 * @param paymentMethod
	 * @param items
	 * @return Sale
	 */
	public static Sale createNormalSale(PriceList priceList, PaymentMethod paymentMethod, ArrayList<OrderLine> items) {
		LocalDate date = LocalDate.now();
		Sale sale = new Sale(priceList, date);	
		
		sale.setItems(items);
		sale.setPaymentMethod(paymentMethod);
		Storage.addSaleToCompletedSales(sale);
		return sale;
	}
	
	/**
	 * Metoden opretter et fridayBarSale, herefter sættes dato, salg oprettes og afluttes med items og paymentmethod
	 * 
	 * @param priceList
	 * @param paymentMethod
	 * @param items
	 * @return Sale
	 */
	public static Sale createFridayBarSale(PriceList priceList, PaymentMethod paymentMethod, ArrayList<OrderLine> items) {
		LocalDate date = LocalDate.now();
		Sale sale = new Sale(priceList, date);	
		sale.setItems(items);
		sale.setPaymentMethod(paymentMethod);
		
		Storage.addSaleToCompletedSales(sale);

		return sale;
	}
	
	/**
	 * Metoden opretter et tourSale, herefter sættes dato, salg oprettes og sættes til unpaidSales med items 
	 * 
	 * @param dateAndTime
	 * @param tourOrderLine
	 * @return TourSale
	 */
	public static TourSale createTourSale( LocalDateTime dateAndTime, OrderLine tourOrderLine) {
		LocalDate dateOfSale = LocalDate.now();
		TourSale trip = new TourSale(dateOfSale, dateAndTime);
		ArrayList<OrderLine> tmpOL = new ArrayList<OrderLine>();

		tmpOL.add(tourOrderLine);
		
		trip.setItems(tmpOL);
		trip.setPaymentMethod(null);
		trip.setPriceList(Storage.getPriceLists().get(1));
		Storage.addSalesToUnpaidSales(trip);
		
		return trip;
	}
	
	//setters
	/**
	 * Metoden sætter en pris eller overskriver en eksisterende pris på et produkt i prislisten
	 * 
	 * Pre: price >= 0
	 * Pre: Storage.getPriceLists().contains(priceList)
	 * 
	 * @param priceList
	 * @param product
	 * @param price
	 */
	public static void setPriceForProductInPriceList(PriceList priceList,Product product, Double price) {
		Pre.require(price >=0);
		Pre.require(Storage.getPriceLists().contains(priceList));
		priceList.setPrice(product, price);
	}
	
	
	/**
	 * Metoden sætter en pris eller overskriver en eksisterende pris på en produktgruppe i prislisten
	 * 
	 * Pre: price >= 0
	 * Pre: Storage.getPriceLists().contains(priceList)
	 * 
	 * @param priceList
	 * @param productGroup
	 * @param price
	 */
	public static void addPriceToProductGroup(PriceList priceList, ProductGroup productGroup, Double price) {
		Pre.require(price >=0);
		Pre.require(price != null);
		Pre.require(Storage.getPriceLists().contains(priceList));
		
		priceList.setPriceForGroup(productGroup, price);
	}
	
	
	/**
	 * Metoden sætter en given paymentmethod til et salg
	 * 
	 * @param paymentMethod
	 * @param sale
	 */
	public static void setPaymentMethodInSale(PaymentMethod paymentMethod, Sale sale) {
		sale.setPaymentMethod(paymentMethod);
	}
		
	//getters
	/**
	 * Metoden returner et produkts pris fra en given prisliste
	 * 
	 * Pre: product != null
	 * Pre: priceList != null
	 * 
	 * @param product
	 * @param priceList
	 * @return double
	 */
	public static Double getProductPriceFromPriceList(Product product, PriceList priceList) {
		Pre.require(product != null);
		Pre.require(priceList != null);
		Double d = priceList.getPrice(product);
		
		return d;
	}
	
	/**
	 * Metoden returner en arrayliste med produkter i given gruppe
	 * 
	 * Pre: productgroup != null
	 * 
	 * @param productgroup
	 * @return ArrayList<Product>
	 */
	public static ArrayList<Product> getProductsInProductGroup(ProductGroup productgroup) {
		Pre.require(productgroup != null);
		return productgroup.getProducts();
	}
	
	/**
	 * Metoden returner en arrayliste af produktgrupper
	 * 
	 * @return ArrayList<Productgroup>
	 */
	public static ArrayList<ProductGroup> getProductGroups() {
		return new ArrayList<ProductGroup> (Storage.getProductGroups());
	}
	
	
	/**
	 * Metoden returner en arrayliste af prislister
	 * 
	 * @return ArrayList<PriceList>
	 */
	public static ArrayList<PriceList> getPriceLists() {
		return Storage.getPriceLists();
	}
	
	/**
	 * Metoden returner en specifik prisliste
	 * 
	 * Giver en IllegalArgumentException hvis target ikke er fundet
	 * 
	 * Pre: target.length() > 0
	 * 
	 * @param target
	 * @return PriceList
	 */
	public static PriceList getPriceListByName(String target) {
		ArrayList<PriceList> list = Storage.getPriceLists();
		int i = 0;
		while (i < list.size()) {
			String k = list.get(i).toString();
			if (k.equals(target)) {
				return list.get(i);
			}
			i++;
		}
		throw new IllegalArgumentException("Ugyldigt prisliste. Tjek for taste/stavefejl");
	}
	
	/**
	 * Metoden returner en specifik produktgruppe
	 * 
	 * Giver en IllegalArgumentException hvis target ikke er fundet
	 * 
	 * Pre: target.length() > 0
	 * 
	 * @param target
	 * @return ProductGroup
	 */
	public static ProductGroup getProductGroupByName(String target) {
		ArrayList<ProductGroup> list = Storage.getProductGroups();
		int i = 0;
		while (i < list.size()) {
			String k = list.get(i).toString();
			if (k.equals(target)) {
				return list.get(i);
			}
			i++;
		}
		throw new IllegalArgumentException("Ugyldigt prisliste. Tjek for taste/stavefejl");
	}
	
	/**
	 * Metoden returner en specifik produkt
	 * 
	 * Giver en IllegalArgumentException hvis target ikke er fundet
	 * 
	 * Pre: target.length() > 0
	 * 
	 * @param target
	 * @return Product
	 */
	public static Product getProductByName(String target, ProductGroup group) {
		ArrayList<Product> list = group.getProducts();
		int i = 0;
		while (i < list.size()) {
			String k = list.get(i).toString();
			if (k.equals(target)) {
				return list.get(i);
			}
			i++;
		}
		throw new IllegalArgumentException("Ugyldigt prisliste. Tjek for taste/stavefejl");
	}

	/**
	 * Metoden tager enumerationen paymentMethod og laver om til et array
	 * 
	 * @return PaymentMethod
	 */
	public static PaymentMethod[] paymentMethodsToArray() {
		return PaymentMethod.values();
	}
	
	/**
	 * Metoden returner completedSales fra storage
	 * 
	 * @return ArrayList<Sale>
	 */
	public static ArrayList<Sale> getCompletedSales() {
		return Storage.getSalescompleted();
	}
	
	/**
	 * Metoden returner unpaidSales fra storage
	 * 
	 * @return ArrayList<Sale>
	 */
	public static ArrayList<Sale> getUnpaidSales() {
		return Storage.getSalesunpaid();
	}
	
	/**
	 * Metoden returner en arrayliste med oversigt over afsluttede salg i en given periode
	 * 
	 * @param start
	 * @param end
	 * @return ArrayList<Sale> 
	 */
	public static ArrayList<Sale> getCompletedSalesForPeriod(LocalDate start, LocalDate end) {
		ArrayList<Sale> list = new ArrayList();
		for (Sale sale : Storage.getSalescompleted()) {
			LocalDate salesDate = sale.getDateOfSale();
			if (salesDate.isAfter(start.minus(1,ChronoUnit.DAYS)) && salesDate.isBefore(end.plus(1, ChronoUnit.DAYS))) {
				list.add(sale);
			}
		}
		return list;
	}
	
	/**
	 * Metoden returner en arrayliste med oversigt over ikke-afsluttede salg i en given periode
	 * 
	 * @param start
	 * @param end
	 * @return
	 * ArrayList<Sale>
	 */
	public static ArrayList<Sale> getUnpaidSalesForPeriod(LocalDate start, LocalDate end) {
		ArrayList<Sale> list = new ArrayList();
		for (Sale sale : Storage.getSalesunpaid()) {
			LocalDate salesDate = sale.getDateOfSale();
			if (salesDate.isAfter(start.minus(1,ChronoUnit.DAYS)) && salesDate.isBefore(end.plus(1, ChronoUnit.DAYS))) {
				list.add(sale);
			}
		}
		return list;
	}
	
	/**
	 * Metoden returner en integer værdig sværende til antallet af øl købt med ølkort i en given periode
	 * 
	 * @param start
	 * @param end
	 * @return int
	 */
	public static int getBeersSoldWithBeerCard(LocalDate start, LocalDate end) {
		ProductGroup bottledBeers = getProductGroupByName("Flaskeøl");
		ProductGroup tapBeers = getProductGroupByName("Fadøl");
	
		int counter=0;
		
		for (Sale sale :getCompletedSalesForPeriod(start, end)) {
			if(sale.getPaymentMethod().equals(PaymentMethod.BEERCARD)) {
			for (OrderLine orderLine : sale.getItems()) {
					if(orderLine.getProduct().getGroup().equals(bottledBeers) || orderLine.getProduct().getGroup().equals(tapBeers)) {
						counter += orderLine.getAmount();
					}
				}
			}
		}
		return counter;
	}
	
	
	/**
	 * Metoden returner antalet af solgte klip på klippekort i en given periode
	 * 
	 * @param start
	 * @param end
	 * @return int
	 */
	public static int getSoldBeerCardClips(LocalDate start, LocalDate end) {
		ProductGroup productGroup = getProductGroupByName("Klippekort");
		Product beerCard = getProductByName("Klippekort", productGroup);
		int counter=0;
		int numberOfClipsPrCard = 4;
		for(Sale sale : getCompletedSalesForPeriod(start, end)) {
			for (OrderLine orderLine : sale.getItems())	{
				if (orderLine.getProduct().equals(beerCard)) {
					counter += orderLine.getAmount();
				}
			}
		}
		return counter*numberOfClipsPrCard;
	}
	
	/**
	 * Metoden returner en arrayliste med ikke afleverede udlejede produkter
	 * 
	 * @return ArrayList<Product>
	 */
	public static ArrayList<Product> getNonReturnedRentalProducts()	{

		ArrayList products = new ArrayList();
	
		for (Sale sale : getUnpaidSales()) {
			if(sale instanceof TapBeerRental) {
				TapBeerRental tapBeerRental = (TapBeerRental) sale;
			for(OrderLine orderLine : tapBeerRental.getItemsOut()) {
			
				if(orderLine.getProduct().getGroup().isRental()) {
					for(int i =0; i < orderLine.getAmount();i++) {
						products.add(orderLine.getProduct());
					}
				}
			}
			}	
		}
		return products;
	}

	//Storage rearranging
	/**
	 * Metoden tager et ikke afsluttet salg og flytter det til afsluttet salg
	 * 
	 * giver en IllegalArgumentException hvis ikke salget er fundet i listen over ikke afsluttede salg
	 * 
	 * @param sale
	 */
	public static void moveSaleFromUnpaidToCompleted(Sale sale) {
		if(Storage.removeSaleFromUnpaidSales(sale))
			Storage.addSaleToCompletedSales(sale);
		else {
			throw new IllegalArgumentException("Salg ikke fundet i ubetalte salg i Storage");
		}
	}
	
	//calculations
	
	/**
	 * Metoden returner den samlede pris for nogle produkter i en given orderlinje ud fra prisliste
	 * 
	 * @param orderLine
	 * @param priceList
	 * @return double
	 */
	//Calculates the total price of an OrderLine from a given PriceList
	public static double calculatePriceFromOrderLine(OrderLine orderLine, PriceList priceList) {
		double price =0;
		if(orderLine.getSpecialPrice() == null)
			price = priceList.getPrice(orderLine.getProduct())*orderLine.getAmount()* (1-orderLine.getPercentageDiscount()/100);
		else
			price = orderLine.getSpecialPrice()*orderLine.getAmount();
		
		return price;
	}
	
	/**
	 * Metoden finder næste kommende mandag
	 * bruges til at fastlægge afleveringsdag for udlejet udstyr
	 * 
	 * @param dateOut
	 * @return LocalDate
	 */
	public static LocalDate findNextMonday(LocalDate dateOut) {
		 LocalDate nextMonday = dateOut.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
		 return nextMonday;
	}
	
	/**
	 * Metoden udregner det samlede depositum fra en fadølsudlejning ud fra en orderlinje
	 * 
	 * @param orderLines
	 * @return double
	 */
	public static double calculateDeposit(ArrayList<OrderLine> orderLines)	{
		double sum =0;
		for (OrderLine ol : orderLines ) {
			sum += ol.getProduct().getGroup().getDeposit() * ol.getAmount();
		}
		return sum;
	}
	
	/**
	 * Metoden udregner hvor meget af de udlejede ting fra fadølsudlejning som er brugt om kommet tilbage
	 * for derefter at kunne bestemme hvor meget der skal betales for i alt
	 * 
	 * @param itemsIn
	 * @param tapBeerRental
	 * @return ArrayList<OrderLine>
	 */
	public static ArrayList<OrderLine> calculateFinalSaleInTapBeerRental(ArrayList<OrderLine> itemsIn, TapBeerRental tapBeerRental)	{
		return tapBeerRental.calculateFinalSale(itemsIn);
	}
	
	/**
	 * Metoden udregner den totale pris for et givent salg
	 * 
	 * @param sale
	 * @return double
	 */
	public static double calculateTotalPrice(Sale sale)	{
		return sale.calculateTotalPrice();
	}
	
	/**
	 * metoden returner prisen hvor den eller indbetalte pant er trukket fra.
	 * prisen er hvad der skal betales
	 * 
	 * @param tapBeerRental
	 * @return double
	 */
	public static double calculateDifferenceAfterRental(TapBeerRental tapBeerRental) {
		return tapBeerRental.calculateDifference();
	}
	
	
	//init
	/**
	 * Metoden initialiser produkter, produktgrupper, salg, ture og andre ting som basis data for systemet
	 */
	public static void initStorage() {
				
		// oprettelse af produktgrupper
		ProductGroup flaskeØlProductGroup = createProductGroup("Flaskeøl");
		ProductGroup fadØlProductGroup = createProductGroup("Fadøl");
		ProductGroup spiritusProductGroup = createProductGroup("Spiritus");
		ProductGroup fustagerProductGroup = createProductGroup("Fustager");
		ProductGroup rundvisningerProductGroup = createProductGroup("Rundvisninger");
		ProductGroup kulsyreProductGroup = createProductGroup ("Kulsyre");
		ProductGroup fadølsAnlægProductGroup = createProductGroup("Fadølsanlæg");
		ProductGroup sammenpakningerProductGroup = createProductGroup("Sammenpakninger");
		ProductGroup beerCardProductGroup = createProductGroup("Klippekort");
		
		//pant sættes på grupper der skal have pant
		fustagerProductGroup.setDeposit(200);
		kulsyreProductGroup.setDeposit(1000);
		
		//udlejningsproduktgrupper sættes
		fadølsAnlægProductGroup.setIsRental(true);
				
		
		Product klosterbrygProduct = createProduct(flaskeØlProductGroup, "Klosterbryg");
		Product sweetGeorgiBrownProduct = createProduct(flaskeØlProductGroup, "Sweet Georgia Brown");
		Product extraPilsnerProduct = createProduct(flaskeØlProductGroup, "Extra Pilsner");
		Product celebrationProduct = createProduct(flaskeØlProductGroup, "Celebration");
		Product blondieProduct = createProduct(flaskeØlProductGroup, "Blondie");
		Product forårsbrygProduct = createProduct(flaskeØlProductGroup, "Forårsbryg");
		Product indiaPaleAleProduct = createProduct(flaskeØlProductGroup, "India Pale Ale");
		Product julebrygProduct = createProduct(flaskeØlProductGroup, "Julebryg");
		Product juletøndenProduct = createProduct(flaskeØlProductGroup, "Juletønden");
		Product oldstrongaleProduct = createProduct(flaskeØlProductGroup, "Old Strong Ale");
		Product fregattenjyllandProduct = createProduct(flaskeØlProductGroup, "Fregatten Jylland");
		Product imperialstoutProduct = createProduct(flaskeØlProductGroup, "Emperial Stout");
		Product tributeProduct = createProduct(flaskeØlProductGroup, "Tribute");
		Product blackmonsterProduct = createProduct(flaskeØlProductGroup, "Black Monster");
		
		Product klosterbrygFadProduct = createProduct(fadØlProductGroup, "Klosterbryg Fadøl");
		Product jazzClassicFadProduct = createProduct(fadØlProductGroup, "Jazz Classic Fadøl");
		Product ekstraPilsnerFadProduct = createProduct(fadØlProductGroup, "Ekstra Pilsner Fadøl");
		Product celebrationFadProduct = createProduct(fadØlProductGroup, "Celebration Fadøl");
		Product blondieFadProduct = createProduct(fadØlProductGroup, "Blondie Fadøl");
		Product forårsbrygFadProduct = createProduct(fadØlProductGroup, "forårsbryg Fadøl");
		Product inadianPaleAleFadProduct = createProduct(fadØlProductGroup, "Indian Pale Ale Fadøl");
		Product julebrygFadProduct = createProduct(fadØlProductGroup, "Julebryg Fadøl");
		Product imperialStoutFadProduct = createProduct(fadØlProductGroup, "Imperial Stout Fadøl");
		Product specialFadProduct = createProduct(fadØlProductGroup, "Special Fadøl");
		
		Product spiritOfAarhusProduct = createProduct(spiritusProductGroup, "Spirit of Aarhus");
		Product soaMedPindProduct = createProduct(spiritusProductGroup, "SOA med Pind");
		Product whiskeyProduct = createProduct(spiritusProductGroup, "Whiskey");
		Product liqourOfAarhusProduct = createProduct(spiritusProductGroup, "Liqour of Aarhus");
		
		Product klosterBryg20LiterProduct = createProduct(fustagerProductGroup, "Klosterbryg 20 liter");
		Product jazzClassic25Liter = createProduct(fustagerProductGroup, "Jazz Classic 25 liter");	
		Product extraPilsner25Liter = createProduct(fustagerProductGroup, "Extra Pilsner 25 liter");
		Product celebration20LiterProduct = createProduct(fustagerProductGroup, "Celebration 20 liter");
		Product blondie25Liter = createProduct(fustagerProductGroup, "Blondie 25 liter");	
		Product forårsbryg20Liter = createProduct(fustagerProductGroup, "Forårsbryg 20 liter");
		Product indianpaleale20LiterProduct = createProduct(fustagerProductGroup, "Indian Pale Ale 20 liter");
		Product julebryg20Liter = createProduct(fustagerProductGroup, "Julebryg 20 liter");	
		Product imperialstout20Liter = createProduct(fustagerProductGroup, "Imperial Stout 20 liter");
		Product rundvisningsturProduct = createProduct(rundvisningerProductGroup, "Rundvisningstur");
		Product kulsyre6KgProduct = createProduct(kulsyreProductGroup, "Kulsyre 6kg");
		Product fadølsanlægEnkeltHaneProduct = createProduct(fadølsAnlægProductGroup, "Fadølsanløg enkelt hane");
		
		Product beerCardProduct = createProduct(beerCardProductGroup, "Klippekort");
		
		GiftBox gb1 = Controller.createGiftBox("2 øl, 2 glas",sammenpakningerProductGroup, 2,  new ArrayList<Product>());
		
		PriceList fredagsbarPriceList = createPriceList("Fredagsbar");
		PriceList butiksSalgPriceList = createPriceList("Butikssalg");
		
		fredagsbarPriceList.updateProducts(getProductGroups());
		butiksSalgPriceList.updateProducts(getProductGroups());

		fredagsbarPriceList.setPriceForGroup(flaskeØlProductGroup, 50d);
		fredagsbarPriceList.setPriceForGroup(fadØlProductGroup, 30d);
		fredagsbarPriceList.setPrice(spiritOfAarhusProduct,300d);
		fredagsbarPriceList.setPrice(soaMedPindProduct, 350d);
		fredagsbarPriceList.setPrice(whiskeyProduct, 500d);
		fredagsbarPriceList.setPrice(liqourOfAarhusProduct, 175d);
		fredagsbarPriceList.setPrice(beerCardProduct, 100d);
		
		butiksSalgPriceList.setPriceForGroup(flaskeØlProductGroup, 36d);
		butiksSalgPriceList.setPriceForGroup(fadØlProductGroup, null);
		butiksSalgPriceList.setPrice(spiritOfAarhusProduct, 300d);
		butiksSalgPriceList.setPrice(soaMedPindProduct, 350d);
		butiksSalgPriceList.setPrice(whiskeyProduct, 500d);
		butiksSalgPriceList.setPrice(liqourOfAarhusProduct, 175d);
		
		butiksSalgPriceList.setPrice(klosterBryg20LiterProduct, 775d);
		butiksSalgPriceList.setPrice(jazzClassic25Liter, 625d);
		butiksSalgPriceList.setPrice(extraPilsner25Liter, 625d);
		butiksSalgPriceList.setPrice(rundvisningsturProduct, 100d);
		butiksSalgPriceList.setPrice(kulsyre6KgProduct, 400d);
		butiksSalgPriceList.setPrice(fadølsanlægEnkeltHaneProduct, 250d);
		butiksSalgPriceList.setPrice(gb1, 100d);
		
		OrderLine ol1 = createOrderLine(rundvisningsturProduct, 25, null, 0d);
		OrderLine olTapBeer =  createOrderLine(klosterBryg20LiterProduct, 3, null, 0d);
		OrderLine olFadølAnlæg =  createOrderLine(fadølsanlægEnkeltHaneProduct, 1, null, 0d);
		ArrayList<OrderLine> olTapBeerList = new ArrayList();
		olTapBeerList.add(olTapBeer);
		olTapBeerList.add(olFadølAnlæg);
	 
		
		TourSale ts1 = createTourSale( LocalDateTime.now(), ol1);
		TapBeerRental tapBeerRentalTest = createTapBeerRental(butiksSalgPriceList,  olTapBeerList, LocalDate.of(2020, 4, 12), LocalDate.of(2020, 4, 15), true); 
	}
	
}
