package storage;

import java.util.ArrayList;

import model.PriceList;
import model.ProductGroup;
import model.Sale;

public class Storage {
	
	private final static ArrayList<ProductGroup> productGroups = new ArrayList();
	private final static ArrayList<PriceList> priceLists = new ArrayList();
	private final static ArrayList<Sale> salesCompleted = new ArrayList();
	private final static ArrayList<Sale> salesUnpaid = new ArrayList();
		
	public static void addProductGroup(ProductGroup group) {
		if(!productGroups.contains(group)) {
			productGroups.add(group);
		}
	}
	
	public static void addPriceList(PriceList list) {
		if(!priceLists.contains(list)) {
			priceLists.add(list);
		}
	}

	public static void addSalesToUnpaidSales(Sale sale) {
		if(!salesUnpaid.contains(sale)) {
			salesUnpaid.add(sale);
		}
	}
		
	public static ArrayList<ProductGroup> getProductGroups() {
		return new ArrayList<ProductGroup> ( productGroups);
	}
	
	public static ArrayList<PriceList> getPriceLists() {
		return priceLists;
	}

	public static ArrayList<Sale> getSalescompleted() {
		return salesCompleted;
	}

	public static ArrayList<Sale> getSalesunpaid() {
		return salesUnpaid;
	}
	
	public static boolean removeSaleFromUnpaidSales(Sale sale) {
		if (salesUnpaid.contains(sale)) {
			salesUnpaid.remove(sale);
			return true;
		} else
			return false;
	}
	
	public static void removeSaleFromCompletedSales(Sale sale) {
		if (salesCompleted.contains(sale)) 
			salesCompleted.remove(sale);
	}
	public static void removeProductGroup(ProductGroup productGroup) {
		if (productGroups.contains(productGroup)) {
		productGroups.remove(productGroup);
		}
	}
	
	public static void addSaleToCompletedSales(Sale sale) {
		if(!salesCompleted.contains(sale)) 
			salesCompleted.add(sale);
	}
}
