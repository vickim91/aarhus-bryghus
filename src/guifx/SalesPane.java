package guifx;

import java.io.IOException;
import java.time.LocalDate;
import controller.Controller;
import controller.GuiController;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.Product;
import model.Receipt;
import model.Sale;
import model.TapBeerRental;
import model.TourSale;

public class SalesPane extends GridPane {
	
	private ListView<Sale> LvwCompletedSales = new ListView<Sale>();
	private ListView<Sale> LvwUnpaidSales = new ListView<Sale>();
	private Button completeSaleBtn = new Button("Afslut salg");
	private Button printReceiptbtn = new Button("Print Kvittering");
	private Label lblCompletedSales = new Label("Afsluttede salg");
	private Label lblUnpaidSales = new Label("Ikke-afsluttede salg");
	private Label lblStartDate = new Label("Periode start");
	private Label lblEndDate = new Label("Periode slut");
	private DatePicker datePickerStart = new DatePicker();
	private DatePicker datePickerEnd = new DatePicker();
	private TextField txfSoldBeerCards = new TextField();
	private TextField txfBeerSoldWithBeerCard = new TextField();
	private Label lblBeerCardsSold = new Label("solgte klip på klippekort for periode");
	private Label lblBeersSoldWithBeerCard = new Label("brugte klip på klippekort for periode");
	private ListView<Product> lvwNonReturnedRentalProducts = new ListView<>();
	private Label lblNonReturnedRetalProducts = new Label("Ikke afleverede udlejningsprodukter");
	LocalDate start;
	LocalDate end;
	Receipt receipt = new Receipt();
	
	public SalesPane() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);
		
		this.add(lblCompletedSales, 0, 0);
		this.add(lblUnpaidSales, 1, 0);
		this.add(LvwCompletedSales, 0, 1);
		this.add(LvwUnpaidSales, 1, 1);
		this.add(completeSaleBtn, 1, 2);
		this.add(printReceiptbtn, 0, 2);
		LvwUnpaidSales.setPrefWidth(350);
		LvwCompletedSales.setPrefWidth(350);
		this.add(lblStartDate, 2, 2);
		this.add(lblEndDate, 2, 4);
		this.add(datePickerStart, 2, 3);
		this.add(datePickerEnd, 2, 5);
		datePickerStart.setValue(LocalDate.now());
		datePickerEnd.setValue(LocalDate.now());
		
		this.add(lblBeerCardsSold, 0, 3);
		this.add(txfSoldBeerCards, 0, 4);
		this.add(lblBeersSoldWithBeerCard, 1, 3);
		this.add(txfBeerSoldWithBeerCard, 1, 4);
		txfBeerSoldWithBeerCard.setEditable(false);
		txfSoldBeerCards.setEditable(false);
		this.add(lblNonReturnedRetalProducts, 2, 0);
		this.add(lvwNonReturnedRentalProducts, 2, 1);
		
		ChangeListener<LocalDate> startDateListener = (ov, oldValue, newValue) -> dateChanged();
		datePickerStart.valueProperty().addListener(startDateListener);
		
		ChangeListener<LocalDate> endDateListener = (ov, oldValue, newValue) -> dateChanged();
		datePickerEnd.valueProperty().addListener(endDateListener);
				
		completeSaleBtn.setOnAction(e -> afslutAction());
		printReceiptbtn.setOnAction(e -> {
			try {
				printAction();
			} catch (IOException ioe) { 
				ioe.printStackTrace();
			}
		});
		updateControls();	
	}
	
	public void updateControls() {
		dateChanged();
	}
	
	public void dateChanged() {
		 start = datePickerStart.getValue();
		 end = datePickerEnd.getValue();
		
		LvwCompletedSales.getItems().setAll(Controller.getCompletedSalesForPeriod(start, end));
		LvwUnpaidSales.getItems().setAll(Controller.getUnpaidSalesForPeriod(start, end));
		
		txfBeerSoldWithBeerCard.setText(String.valueOf(Controller.getBeersSoldWithBeerCard(start, end)));
		txfSoldBeerCards.setText(String.valueOf(Controller.getSoldBeerCardClips(start, end)));
		lvwNonReturnedRentalProducts.getItems().clear();
		lvwNonReturnedRentalProducts.getItems().setAll(Controller.getNonReturnedRentalProducts());
	}
		
	public void afslutAction() {
		Sale sale =LvwUnpaidSales.getSelectionModel().getSelectedItem();
		if(sale == null) {
			GuiController.noValueSelectedAlert("Der skal vælges en ordrer for at kunne fjerne den");
		} else {
			if(sale instanceof TapBeerRental) {
				TapBeerRentalEndSaleWindow tapBeerEndSaleWindow = new TapBeerRentalEndSaleWindow("afslut udlejning", (TapBeerRental) sale);
				tapBeerEndSaleWindow.showAndWait();
				System.out.println(Controller.getCompletedSalesForPeriod(start, end));
				System.out.println(Controller.getUnpaidSalesForPeriod(start, end));
			}
			
			if(sale instanceof TourSale) {
				TourSaleEndSaleWindow tourSaleEndSaleWindow = new TourSaleEndSaleWindow("Afslut salg");
				tourSaleEndSaleWindow.setSale(sale);
				tourSaleEndSaleWindow.showAndWait();
			}
			updateControls();
		}
	}
	
	public void printAction() throws IOException {
		receipt.print();
	}
}
