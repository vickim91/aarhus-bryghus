package guifx;

import controller.Controller;
import controller.GuiController;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.PaymentMethod;
import model.Sale;

public class TourSaleEndSaleWindow extends Stage {
	
	private Label lblPrice = new Label();
	private Button buttonConfirm = new Button("Afslut");
	private ComboBox cbbPaymentMethod = new ComboBox();
	private VBox container = new VBox();
	private Sale tempSale;
	
	public TourSaleEndSaleWindow(String title) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);
		this.setTitle(title);
		GridPane pane = new GridPane();
	
		Scene scene = new Scene(pane, 210, 150);
		this.setScene(scene);

		initContent(pane);
	}
	
	public void initContent(GridPane pane) {
		lblPrice.setStyle("-fx-font-size: 16px");
		buttonConfirm.setPrefSize(100, 45);
		buttonConfirm.setStyle("-fx-font-size: 16px");
		buttonConfirm.setOnAction(e -> completeeSaleAction());
		cbbPaymentMethod.setPromptText("Vælg Betalings metode");
		cbbPaymentMethod.setItems(FXCollections.observableArrayList(Controller.paymentMethodsToArray()));
		cbbPaymentMethod.getItems().remove(PaymentMethod.BEERCARD); // Fjerner ugyldig betalingsmetode - øl-kort.
		
		container.setPadding(new Insets(20));
		container.setAlignment(Pos.TOP_CENTER);
		container.setSpacing(15);
		container.getChildren().addAll(lblPrice, cbbPaymentMethod, buttonConfirm);
		pane.add(container, 0, 0);
	}
	
	public void setSale(Sale sale) {
		this.tempSale = sale;
		lblPrice.setText("Pris: "+ String.valueOf(tempSale.calculateTotalPrice()));
	}
	
	public void completeeSaleAction() {
		PaymentMethod paymentMethod = (PaymentMethod) cbbPaymentMethod.getSelectionModel().getSelectedItem();
	
		if(paymentMethod == null) {
			GuiController.noValueSelectedAlert("Der skal angives en betalingsmetode");
		} else {
			Controller.setPaymentMethodInSale(paymentMethod, tempSale);
			Controller.moveSaleFromUnpaidToCompleted(tempSale);
			this.close();
		}
	}
}
