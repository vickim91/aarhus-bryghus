package guifx;

import java.util.ArrayList;
import controller.Controller;
import controller.GuiController;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import model.PriceList;
import model.Product;
import model.ProductGroup;

public class ProductPane extends GridPane implements Subject {

	private ListView<ProductGroup> lvwProductGroup = new ListView<ProductGroup>();
	private ListView<Product> lvwProducts = new ListView<Product>();
	private ArrayList<PriceList> pricelists = Controller.getPriceLists();
	
	private TextField txfNavn = new TextField();

	private HBox hboxbuttom = new HBox();
	
	private Label lblProductGroup = new Label("Produkt gruppe:");
	private Label lblProduct = new Label("Produkt:");
	
	private Button groupButton = new Button("Tilføj til Gruppe");
	
	private CreateGiftBoxWindow window = new CreateGiftBoxWindow("Angiv antal");
		
	public ProductPane() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);
		lvwProductGroup.getItems().setAll(Controller.getProductGroups());
		lvwProductGroup.setMaxHeight(300);
		lvwProducts.setMaxHeight(300);
		txfNavn.setPromptText("Navn på Produkt");
		groupButton.setOnAction(event -> this.addAction());

		this.add(lblProductGroup, 0, 0);
		this.add(lvwProductGroup, 0, 1);
		this.add(lvwProducts, 1, 1);

		ChangeListener<ProductGroup> CLP = (ov, oldValue, newValue) -> this.selectedProductChanged();
		this.lvwProductGroup.getSelectionModel().selectedItemProperty().addListener(CLP);

		lvwProductGroup.getItems().setAll(Controller.getProductGroups());

		hboxbuttom.setSpacing(10);
		hboxbuttom.getChildren().add(groupButton);
		this.add(txfNavn, 1, 3);
		this.add(lblProduct, 1, 0);
		this.add(hboxbuttom, 1, 5);
	}

	private void initContent(GridPane pane) {
		Controller.initStorage();
	}
	
	public void addAction() {
		String tmp = txfNavn.getText();
		Product proObj = null;
		
		if(	!tmp.trim().isEmpty()) {	
			ProductGroup proG1 = this.lvwProductGroup.getSelectionModel().getSelectedItem();
			if(proG1.toString().equals("Sammenpakninger")) {
				window.showAndWait();
				proObj = Controller.createGiftBox(tmp, proG1, window.getNumberOfBeers(), new ArrayList<Product>());
			}
			else {
			proObj = Controller.createProduct(proG1, tmp);
			}
			
			notifyObservers(proObj);
			updateControls();
		} else {
			GuiController.noValueSelectedAlert("Der skal angives et navn");
		}
	}
	
	private void selectedProductChanged() {
		this.updateControls();
	}

	public void updateControls() {
		ProductGroup proG1 = this.lvwProductGroup.getSelectionModel().getSelectedItem();

				
		if(proG1.equals(Controller.getProductGroupByName("Sammenpakninger"))) {
			lvwProducts.getItems().setAll(GuiController.returnGiftBoxTemplates(proG1));
		} else {
			lvwProducts.getItems().setAll(Controller.getProductsInProductGroup(proG1));
		}
	}
	
	@Override
	public void notifyObservers(Product product) {		
		for (PriceList priceList : pricelists) {
			Controller.addProductToPriceList(product, priceList);
		}
	}
}
