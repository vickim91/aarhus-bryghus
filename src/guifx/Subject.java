package guifx;

import model.Product;

public interface Subject {
	public void notifyObservers(Product product);
}
