package guifx;

import java.util.ArrayList;
import controller.Controller;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.OrderLine;
import model.PaymentMethod;
import model.PriceList;
import model.Sale;

public class FridayBarWindow extends Stage {
	
	private Button cash = new Button("Kontant");
	private Button credit = new Button("Kredit Kort");
	private Button beercard = new Button("Øl kort");
	private Button mpay = new Button("MobilePay");
	private Label price = new Label();
	private VBox container = new VBox();
	private ArrayList<OrderLine> orderLines = new ArrayList<>();
	
	public FridayBarWindow(String title) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.setTitle(title);
		GridPane pane = new GridPane();
		initContent(pane);
		
		Scene scene = new Scene(pane, 150, 260);
		this.setScene(scene);
	}
	
	public void initContent(GridPane pane) {
		container.setAlignment(Pos.CENTER);
		container.getChildren().addAll(price, cash, credit, beercard, mpay);
		container.setSpacing(10);
		
		price.setStyle("-fx-font-size: 24px; -fx-font-color: black");
				
		mpay.setStyle("-fx-background-color: #6e94d1; -fx-font-size: 18px");
		cash.setStyle("-fx-background-color: #6aed5c; -fx-font-size: 18px");
		credit.setStyle("-fx-background-color: #ffbf51; -fx-font-size: 18px");
		beercard.setStyle("-fx-background-color: #f64af9; -fx-font-size: 18px");
		
		cash.setPrefWidth(150);
		credit.setPrefWidth(150);
		beercard.setPrefWidth(150);
		mpay.setPrefWidth(150);
		
		cash.setPrefHeight(50);
		credit.setPrefHeight(50);
		beercard.setPrefHeight(50);
		mpay.setPrefHeight(50);
		
		cash.setOnAction(e -> confirmAction(PaymentMethod.CASH));
		credit.setOnAction(e -> confirmAction(PaymentMethod.CREDITCARD));
		beercard.setOnAction(e -> confirmAction(PaymentMethod.BEERCARD));
		mpay.setOnAction(e -> confirmAction(PaymentMethod.MOBILEPAY));
			
		pane.add(container, 0, 1);
	}
	
	public void confirmAction(PaymentMethod paymentMethod) {		
		PriceList priceList = Controller.getPriceListByName("Fredagsbar");		
		Sale sale = Controller.createFridayBarSale(priceList, paymentMethod, orderLines);
		this.close();
	}
	
	public void setOrderLines(ArrayList<OrderLine> orderLines) {
		this.orderLines = orderLines;
		price.setText(String.valueOf(orderLineTotal()));
	}
	
	public double orderLineTotal() {
		double tmp = 0;
		
		for (OrderLine orderLine : orderLines) {
			tmp += Controller.calculatePriceFromOrderLine(orderLine, Controller.getPriceLists().get(0));
		}
		
		return tmp;
	}
}
