package guifx;

import controller.Controller;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainApp extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void init() {
		Controller.initStorage();
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Basissystem");
		BorderPane pane = new BorderPane();
		this.initContent(pane);

		Scene scene = new Scene(pane, 1080, 720);
		stage.setScene(scene);
		stage.show();
	}

	// -------------------------------------------------------------------------

	private void initContent(BorderPane pane) {
		TabPane tabPane = new TabPane();
		this.initTabPane(tabPane);
		pane.setCenter(tabPane);
	}

	private void initTabPane(TabPane tabPane) {
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		Tab tabTourSale = new Tab("Salg af rundvisning");
		tabPane.getTabs().add(tabTourSale);

		TourSalePane tourPane = new TourSalePane();
		tabTourSale.setContent(tourPane);
		tabTourSale.setOnSelectionChanged(event -> tourPane.updateControls());
		
		Tab tabNormalSalesMain = new Tab("Opret Normal Salg");
		tabPane.getTabs().add(tabNormalSalesMain);

		NormalSalesPane normPane = new NormalSalesPane();
		tabNormalSalesMain.setContent(normPane);
		tabNormalSalesMain.setOnSelectionChanged(event -> normPane.updateControls());
				
		Tab tabTapBeerRental = new Tab("Udlejning af fadøl");
		tabPane.getTabs().add(tabTapBeerRental);

		TapBeerRentalPane tapBeerPane = new TapBeerRentalPane();
		tabTapBeerRental.setContent(tapBeerPane);
		tabTapBeerRental.setOnSelectionChanged(event -> tapBeerPane.updateControls());
		
		Tab tabGuiMain = new Tab("Produkt oversigt");
		tabPane.getTabs().add(tabGuiMain);

		ProductPane ProductPane = new ProductPane();
		tabGuiMain.setContent(ProductPane);
		
		Tab tabGuiMain2 = new Tab("Prislister");
		tabPane.getTabs().add(tabGuiMain2);

		PricelistPane PricePane = new PricelistPane();
		tabGuiMain2.setContent(PricePane);
		
		tabGuiMain2.setOnSelectionChanged(event -> PricePane.paneChanged());
		
		Tab tabSalesPane = new Tab("Salgslister");
		tabPane.getTabs().add(tabSalesPane);
		
		SalesPane salesPane = new SalesPane();
		tabSalesPane.setContent(salesPane);
		
		tabSalesPane.setOnSelectionChanged(event -> salesPane.updateControls());
		
		Tab tabFridayBarPane = new Tab("Fredagsbar");
		tabPane.getTabs().add(tabFridayBarPane);
		
		FridayBarPane FridayBarPane = new FridayBarPane();
		tabFridayBarPane.setContent(FridayBarPane);
		
		tabFridayBarPane.setOnSelectionChanged(event -> FridayBarPane.updateControls());
	} 
}
