package guifx;

import java.time.LocalDate;
import java.util.ArrayList;
import controller.Controller;
import controller.GuiController;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.OrderLine;
import model.PriceList;

public class TapBeerRentalPane extends GridPane {
	
	private Button buttonOrderLine = new Button();
	private Button buttonSale = new Button();

	private TapBeerOrderLineWindow orderLineWindow = new TapBeerOrderLineWindow("Opret ordrelinje");
	
	private ListView<OrderLine> lvwOrderLines = new ListView();
	private ListView<Double> lvwPrices = new ListView();
		
	private ComboBox cbbPriceLists = new ComboBox();
	
	private Label lbllvw = new Label();
	private Label lbllvw2 = new Label();
	private Label lblcb1 = new Label();

	private Label lblDPOut = new Label();
	private Label lblDPIn  = new Label();
	private Label lblDeposit = new Label();
	
	private DatePicker datepickerOut = new DatePicker();
	private DatePicker datepickerIn = new DatePicker();
	private CheckBox cbDelivery = new CheckBox("levering");
	
	private PriceList lastSelectedPriceList;
	private TextField txfDeposit = new TextField(); 
		

	public TapBeerRentalPane() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);
		
		lbllvw.setText("Ordrelinjer");
		lbllvw2.setText("pris ifølge prisliste");
		lvwOrderLines.setPrefWidth(350);
		lvwPrices.setPrefWidth(150);
		lblcb1.setText("Valg Prisliste:");

		lblDPOut.setText("Dato for begyndelse af leje");
		lblDPIn.setText("Dato for afslutning af leje");
		lblDeposit.setText("Pant");	
		
		buttonOrderLine.setText("Tilføj ordrelinie");
		buttonOrderLine.setOnAction(event -> this.orderLineAction());
		buttonSale.setText("Opret Salg");
		buttonSale.setOnAction(event -> this.createSaleAction());				
		
		this.add(lbllvw, 0, 0);
		this.add(lvwOrderLines, 0, 1);
		this.add(lbllvw2, 1, 0);
		this.add(lvwPrices, 1, 1);
		
		this.add(lblcb1, 0, 2);
		this.add(cbbPriceLists, 0, 3);
		
		cbbPriceLists.getItems().setAll((Controller.getPriceLists()));
		cbbPriceLists.getItems().remove(Controller.getPriceListByName("Fredagsbar"));  //fjerner fredagsbarsprislisten fra tilgængelige prislister
		cbbPriceLists.getSelectionModel().select(0);
		cbbPriceLists.setPrefWidth(200);
		
		ChangeListener<PriceList> priceListListener = (ov, oldPriceList, newPriceList) -> this.selectedPriceListChanged();
		this.cbbPriceLists.getSelectionModel().selectedItemProperty().addListener(priceListListener);
		
		this.add(buttonOrderLine, 1, 2);
		this.add(buttonSale, 1, 5);
		this.add(lblDPOut, 2, 0);
		this.add(datepickerOut, 2, 1);
		this.add(lblDPIn, 2, 3);
		this.add(datepickerIn, 2, 4);
		this.add(cbDelivery, 2, 5);
		this.add(lblDeposit, 2, 6);
		this.add(txfDeposit, 2, 7);
		txfDeposit.setEditable(false);
		
		datepickerOut.setValue(LocalDate.now());
		cbDelivery.setSelected(true);
		
		ChangeListener<Boolean> isDeliveryListener = (ov, oldValue, newValue) -> deliveryStatusChanged(cbDelivery.isSelected());
		cbDelivery.selectedProperty().addListener(isDeliveryListener);
		ChangeListener<LocalDate> dateOutListener = (ov, oldValue, newValue) -> dateOutChanged();
		datepickerOut.valueProperty().addListener(dateOutListener);;
	}
	
	public void updatePrices() {
		if(cbbPriceLists.getSelectionModel().getSelectedItem() != null 	&& lvwOrderLines.getItems().size() > 0) {
			PriceList priceList = (PriceList) cbbPriceLists.getSelectionModel().getSelectedItem();	
			lvwPrices.getItems().clear();
			
			for (OrderLine od : lvwOrderLines.getItems()) {
				lvwPrices.getItems().add(Controller.calculatePriceFromOrderLine(od, priceList));
			}
			double deposit = Controller.calculateDeposit( new ArrayList (lvwOrderLines.getItems()));
			txfDeposit.setText(String.valueOf(deposit));
		}
	}
	
	public void deliveryStatusChanged(boolean isDelivery) {
		if(!isDelivery) {
			LocalDate dateOut = datepickerOut.getValue();
			datepickerIn.setEditable(true);
			datepickerIn.setValue(Controller.findNextMonday(dateOut));
		} else {
			datepickerIn.setEditable(false);
			datepickerIn.getEditor().clear();
		}
	}
	
	public void dateOutChanged() {
		LocalDate dateOut = datepickerOut.getValue();
		if(!cbDelivery.isSelected()) {
			datepickerIn.setValue(Controller.findNextMonday(dateOut));
		}
	}
	
	public void selectedPriceListChanged() {
		PriceList priceList = (PriceList) cbbPriceLists.getSelectionModel().getSelectedItem();
		ArrayList<OrderLine> orderLines = new ArrayList(lvwOrderLines.getItems());
		if (GuiController.checkIfPriceListIsValid(priceList, orderLines)) {
			updatePrices();
			lastSelectedPriceList = (PriceList) cbbPriceLists.getSelectionModel().getSelectedItem();
		} else {
			cbbPriceLists.getSelectionModel().select(lastSelectedPriceList);
			GuiController.noValueSelectedAlert("Den valgte prisliste indeholder ikke nogen pris \n" + "for et eller flere af de valgte ordrelinjers produkter");
		}
	}
	
	public void orderLineAction()  {
		PriceList priceList = (PriceList) cbbPriceLists.getSelectionModel().getSelectedItem();
		
		if(priceList !=  null) {
			orderLineWindow.setPriceList(priceList);
			orderLineWindow.showAndWait();
		} else {
			GuiController.noValueSelectedAlert("Der skal angives en prisliste");
		}
		
		if(orderLineWindow.getOrderLine() !=null && !lvwOrderLines.getItems().contains(orderLineWindow.getOrderLine()))
			lvwOrderLines.getItems().add(orderLineWindow.getOrderLine());
		
		updatePrices();
	}
		
	public void createSaleAction() {
		PriceList priceList = (PriceList) cbbPriceLists.getSelectionModel().getSelectedItem();
		ObservableList<OrderLine> itemsOut = lvwOrderLines.getItems();
		LocalDate dateOut = datepickerOut.getValue();
		LocalDate dateIn = datepickerIn.getValue();
		boolean isDelivery = cbDelivery.isSelected();
		double deposit=0;
		
		if(!txfDeposit.getText().isEmpty())
			deposit = Double.parseDouble( txfDeposit.getText().trim());
		
		if(lvwOrderLines.getItems().isEmpty()) {
			GuiController.noValueSelectedAlert("Der skal angives ordrelinjer før at salget kan oprettes");
		}
		
		if(!GuiController.checkifRentalDateIsValid(dateOut, dateIn)) {
			GuiController.InvalidDateSelectedAlert("dato for udlejning skal være før aflevering og kan ikke være før idag");
		}
		
		ArrayList itemsOutArrayList = new ArrayList(itemsOut);
		Controller.createTapBeerRental(priceList,  itemsOutArrayList, dateIn, dateOut, isDelivery);

		lvwOrderLines.getItems().clear();
		lvwPrices.getItems().clear();
	}
	
	public void updateControls() {
		cbbPriceLists.getItems().setAll(Controller.getPriceLists());
		lvwOrderLines.getItems().clear();
		lvwPrices.getItems().clear();
	}
}
