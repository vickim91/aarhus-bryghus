package guifx;

import java.util.ArrayList;
import controller.Controller;
import controller.GuiController;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.GiftBox;
import model.OrderLine;
import model.PriceList;
import model.Product;
import model.ProductGroup;

public class NormalSalesOrderLineWindow extends Stage {

	private HBox hbox1 = new HBox();
	private HBox hbox2 = new HBox();
	private HBox hbox3 = new HBox();
	private HBox hbox4 = new HBox();
	private HBox hbox5 = new HBox();
	private HBox hbox6 = new HBox();
	private OrderLine orderLine;
	
	private GiftBoxWindow giftBoxWindow = new GiftBoxWindow("Vælg gaveæskeprodukter");
	
	private ComboBox<ProductGroup> cbbProductGroup = new ComboBox();
	private ComboBox cbbProduct = new ComboBox();
	
	private Button buttonFinish = new Button();
	
	private Label lblSpecialPrice = new Label();
	private Label lblSpecialDiscount = new Label();
	private Label lblAmount = new Label();
	
	private TextField txfAmount = new TextField();
	private TextField txfSpecialPrice = new TextField();
	private TextField txfSpecialDiscount = new TextField();
	
	private PriceList priceList;
	private ArrayList<ProductGroup> productGroups = new ArrayList<>();
	private ArrayList<Product> products = new ArrayList<Product>();
	
	public NormalSalesOrderLineWindow(String title) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane, 150, 150);
		this.setScene(scene);
	}
	
	private void initContent(GridPane pane) {
		cbbProductGroup.setPrefWidth(150);
		cbbProduct.setPrefWidth(150);
		buttonFinish.setText("Færdig");
		txfAmount.setPrefWidth(35);
		txfSpecialPrice.setPrefWidth(75);
		txfSpecialDiscount.setPrefWidth(75);
		lblSpecialPrice.setText("Særlig Pris:");
		lblSpecialDiscount.setText("Særlig Rabat:");
		lblAmount.setText("Antal:");
		
		hbox1.getChildren().add(cbbProductGroup);
		hbox2.getChildren().addAll(cbbProduct);
		hbox3.getChildren().addAll(lblAmount, txfAmount);
		hbox4.getChildren().add(buttonFinish);
		hbox5.getChildren().addAll(lblSpecialPrice, txfSpecialPrice);
		hbox6.getChildren().addAll(lblSpecialDiscount, txfSpecialDiscount);
		hbox1.setAlignment(Pos.CENTER);
		hbox2.setAlignment(Pos.CENTER);
		hbox3.setAlignment(Pos.CENTER);
		hbox4.setAlignment(Pos.CENTER);
		hbox5.setAlignment(Pos.CENTER_RIGHT);
		hbox6.setAlignment(Pos.CENTER_RIGHT);
		
		cbbProductGroup.setPromptText("Vælg Kategori");
		cbbProduct.setPromptText("Vælg Produkt");
		
		ChangeListener<Product> productListener = (ov, oldGroup, newGroup) -> this.checkProduct();
		cbbProduct.getSelectionModel().selectedItemProperty().addListener(productListener);
		
		ChangeListener<ProductGroup> productGroupListener = (ov, oldGroup, newGroup) -> this.updateControls();
		cbbProductGroup.getSelectionModel().selectedItemProperty().addListener(productGroupListener);

		buttonFinish .setOnAction(Event -> addAction());
		
		pane.add(hbox1, 0, 0);
		pane.add(hbox2, 0, 1);
		pane.add(hbox3, 0, 4);
		pane.add(hbox4, 0, 5);
		pane.add(hbox5, 0, 2);
		pane.add(hbox6, 0, 3);
	}
	
	private void checkProduct() {
		Product product = (Product) cbbProduct.getSelectionModel().getSelectedItem();
		
		if(product instanceof GiftBox) {
			giftBoxWindow.updateControls((GiftBox) product);
			giftBoxWindow.showAndWait();
			ArrayList<Product> products = new ArrayList<Product>(giftBoxWindow.getProductsChosen());
			txfAmount.setVisible(false);
			System.out.println("kører checkprodukt. giftboxes; " + product);
			GiftBox giftBox = Controller.createGiftBox(product.getName(), product.getGroup(), ((GiftBox) product).getProducts().size(), new ArrayList<Product>());			
			
			giftBox.setProducts(products);
			
			Controller.updatePriceList(priceList);
			
			Controller.setPriceForProductInPriceList(priceList, giftBox, Controller.getProductPriceFromPriceList(product, priceList));
			
			lblAmount.setText(giftBox.getProducts().toString());
		}
	}

	public void updateControls() {
		products.clear();
	
		ProductGroup pg1 = (ProductGroup) cbbProductGroup.getSelectionModel().getSelectedItem();	
		if(pg1 != null)	{
			if(pg1.toString().equals("Sammenpakninger")) {
				products = GuiController.returnGiftBoxTemplatesMatchedWithPriceList(pg1, priceList);
			} else {
				products = GuiController.matchViewWithPriceList(pg1, this.priceList);
			}
			cbbProduct.getItems().setAll(products);
		}
	}
	
	public void addAction() {
		Product pro1;
		if(!(cbbProduct.getSelectionModel().getSelectedItem() instanceof GiftBox)) {
			pro1 = (Product) cbbProduct.getSelectionModel().getSelectedItem();
		}
		else {
			pro1 = Controller.getProductsInProductGroup(Controller.getProductGroupByName("Sammenpakninger")).get(Controller.getProductsInProductGroup(Controller.getProductGroupByName("Sammenpakninger")).size()-1);
		}
		
		int amount = 0;
		if(!(cbbProduct.getSelectionModel().getSelectedItem() instanceof GiftBox) && txfAmount.getText().trim().length() == 0 || !GuiController.checkIfNumbers(txfAmount.getText().trim())) {
			GuiController.noValueSelectedAlert("Der skal angives et gyldigt antal");
		}
		else if(!(cbbProduct.getSelectionModel().getSelectedItem() instanceof GiftBox)) {
			amount = Integer.parseInt(txfAmount.getText().trim());
		}
		else if(cbbProduct.getSelectionModel().getSelectedItem() instanceof GiftBox) {
			amount = 1;
		}		
		
		Double specialPrice = null; 
		
		if(txfSpecialPrice.getText().trim().length()>0 && GuiController.checkIfNumbers(txfSpecialPrice.getText().trim()))
			specialPrice =  Double.parseDouble(txfSpecialPrice.getText().trim());		

		Double percentageDiscount = 0d;  
		
		if(txfSpecialDiscount.getText().trim().length()>0 && GuiController.checkIfNumbers(txfSpecialDiscount.getText().trim()))
			percentageDiscount =  Double.parseDouble(txfSpecialDiscount.getText().trim()); 
		
		if(pro1 == null) {
			GuiController.noValueSelectedAlert("Der skal vælges et produkt");
		} else {
			orderLine =Controller.createOrderLine(pro1, amount, specialPrice, percentageDiscount);
		  this.close();
		}
	}
		  
	public OrderLine getOrderLine() {
		return orderLine;
	}
	
	public void clearWindow() {
		cbbProductGroup.getSelectionModel().clearSelection();
		cbbProduct.getSelectionModel().clearSelection();
		txfAmount.clear();
		txfSpecialPrice.clear();
		txfSpecialDiscount.clear();
		txfAmount.setVisible(true);
		lblAmount.setText("Antal:");
		orderLine = null;
		products.clear();
	}
	
	public void setPriceList(PriceList priceList) {
		this.priceList = priceList;
		updateControls();
		cbbProductGroup.getItems().setAll(Controller.getProductGroups());	
	}
}
