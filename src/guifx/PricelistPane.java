package guifx;

import controller.Controller;
import controller.GuiController;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.PriceList;
import model.Product;
import model.ProductGroup;

public class PricelistPane extends GridPane {
	
	ListView<PriceList> lvwPriceList = new ListView();
	ListView<ProductGroup> lvwProductGroup = new ListView();
	ListView<Product> lvwProduct = new ListView();
	TextField txfGroupPrice = new TextField();
	TextField txfProductPrice = new TextField();
	TextField txfPriceList = new TextField();
		
	public PricelistPane() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);
		
		Label lblProductGroup = new Label("Product group:");
		Label lblProduct = new Label("Product:");
		Label lblPriceList = new Label("Price list: ");
	
		lvwProductGroup.setMaxHeight(300);
		lvwPriceList.setMaxHeight(300);
		lvwProduct.setMaxHeight(300);
		txfGroupPrice.setMaxWidth(100);
		txfProductPrice.setMaxWidth(100);

		Button buttonPriceGroup = new Button("sæt pris for gruppe");
		Button buttonPrice = new Button("sæt pris");
		Button buttonCreatePriceList = new Button("opret prisliste");
		txfGroupPrice.setPromptText("SlutDen()");
		txfProductPrice.setPromptText("SlutDen()");
		txfPriceList.setPromptText("navn på prisliste");
		this.add(lvwPriceList, 0, 1);
		this.add(lvwProduct, 2, 1);
		this.add(lvwProductGroup, 1, 1);
				
		this.add(lblProductGroup, 1, 0);
		this.add(txfGroupPrice, 1, 3);
		this.add(txfProductPrice, 2, 3);
		this.add(lblProduct, 2, 0);
		this.add(lblPriceList, 0, 0);
		this.add(buttonPriceGroup, 1, 5);
		this.add(buttonPrice, 2, 5);
		this.add(txfPriceList, 0, 2);
		this.add(buttonCreatePriceList, 0, 3);
		ChangeListener<PriceList> priceListListener = (ov, oldList, newList) -> this.updateControls();
		lvwPriceList.getSelectionModel().selectedItemProperty().addListener(priceListListener);
		
		ChangeListener<ProductGroup> productGroupListener = (ov, oldGroup, newGroup) -> this.updateControls();
		
		lvwProductGroup.getSelectionModel().selectedItemProperty().addListener(productGroupListener);
		
		ChangeListener<Product> productListener = (ov, oldProduct, newProduct) -> selectedProductChanged();
		
		lvwProduct.getSelectionModel().selectedItemProperty().addListener(productListener);

		buttonPrice.setOnAction(event -> this.addPriceSingleAction());
		buttonPriceGroup.setOnAction(event -> this.addPriceGroupAction());
		buttonCreatePriceList.setOnAction(event -> this.addPriceList());
	}
	

	public void addPriceSingleAction()	{
		PriceList plTemp = lvwPriceList.getSelectionModel().getSelectedItem();	
		ProductGroup pgTemp = lvwProductGroup.getSelectionModel().getSelectedItem();
		Product pTemp = lvwProduct.getSelectionModel().getSelectedItem();
		
		if(GuiController.checkIfNumbers(txfProductPrice.getText().trim()) && !txfProductPrice.getText().isEmpty()) {
		
			double priceTemp = Double.parseDouble(txfProductPrice.getText().trim());
		
			if(plTemp != null && pgTemp !=null && pTemp != null) {
				Controller.setPriceForProductInPriceList(plTemp, pTemp, priceTemp);		
			} 
		} else {
			GuiController.invalidInputAlert("Der skal sættes en pris");
		}
	}
	
	public void addPriceGroupAction() {
		PriceList plTemp = lvwPriceList.getSelectionModel().getSelectedItem();	
		ProductGroup pgTemp = lvwProductGroup.getSelectionModel().getSelectedItem();
		
		if(GuiController.checkIfNumbers(txfGroupPrice.getText().trim()) && !txfGroupPrice.getText().isEmpty()) {
			double priceTemp = Double.parseDouble(txfGroupPrice.getText().trim());
		
				if(plTemp != null && pgTemp !=null) {
					Controller.addPriceToProductGroup(plTemp, pgTemp, priceTemp);
				} 
		} else {
			GuiController.noValueSelectedAlert("Der skal angives et tal som pris");			
		}
	}
	
	public void selectedProductChanged() {
		PriceList plTemp = lvwPriceList.getSelectionModel().getSelectedItem();
		Product pTemp = lvwProduct.getSelectionModel().getSelectedItem();
		
		if(pTemp != null && plTemp != null) {
			String s = String.valueOf( plTemp.getPrice(pTemp));
			txfProductPrice.setText(s);
		}
	}
	
	public void addPriceList() {
		if(txfPriceList.getText().trim().isEmpty()) {
			GuiController.noValueSelectedAlert("Der skal angives et navn til prislisten før den kan oprettes");
		} else {
			PriceList priceList = Controller.createPriceList(txfPriceList.getText().trim());
			Controller.updatePriceList(priceList);
			lvwPriceList.getItems().setAll(Controller.getPriceLists());
		}
	}
	
	public void updateControls() {
						
		ProductGroup pgTemp = lvwProductGroup.getSelectionModel().getSelectedItem();
		if(pgTemp != null) {
			if(pgTemp.toString().equals("Sammenpakninger")) {
				lvwProduct.getItems().setAll(GuiController.returnGiftBoxTemplates(pgTemp));
			} else {
				lvwProduct.getItems().setAll(lvwProductGroup.getSelectionModel().getSelectedItem().getProducts());
			}
		} else {
			lvwProduct.getItems().clear();
			}
		txfProductPrice.clear();
	}


	public void paneChanged() {
		lvwPriceList.getItems().setAll(Controller.getPriceLists());
		lvwProductGroup.getItems().setAll(Controller.getProductGroups());
		
		if(lvwPriceList.getItems().size()>0) {
			lvwPriceList.getSelectionModel().select(0);
		}
		
		if (lvwProductGroup.getItems().size() > 0) {
			 lvwProductGroup.getSelectionModel().select(0);
			lvwProduct.getItems().setAll(lvwProductGroup.getSelectionModel().getSelectedItem().getProducts());
			if(lvwProduct.getItems().size()>0) {
				 lvwProduct.getSelectionModel().select(0);
			}
		}
		updateControls();
	}	
}
