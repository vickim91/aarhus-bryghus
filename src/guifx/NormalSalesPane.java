package guifx;

import java.util.ArrayList;
import controller.Controller;
import controller.GuiController;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import model.OrderLine;
import model.PaymentMethod;
import model.PriceList;
import model.Sale;

public class NormalSalesPane extends GridPane {
	
	private Button buttonOrderLine = new Button();
	private Button buttonFinish = new Button();
	private NormalSalesOrderLineWindow orderLineWindow = new NormalSalesOrderLineWindow("Normal Salg");
	
	private ListView<OrderLine> lvwOrderLines = new ListView();
	private ListView<Double> lvwPrices = new ListView();
		
	private ComboBox cbbPriceLists = new ComboBox();
	private ComboBox cbbPaymentMethod = new ComboBox();
	
	private Label lbllvw = new Label();
	private Label lbllvw2 = new Label();
	private Label lblcb1 = new Label();
	private Label lblcb2 = new Label();

	private PriceList lastSelectedPriceList;
		
	public NormalSalesPane() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);
		
		lbllvw.setText("Ordrelinjer");
		lbllvw2.setText("pris ifølge prisliste");
		lvwOrderLines.setPrefWidth(350);
		lvwPrices.setPrefWidth(150);
		lblcb1.setText("Valg Prisliste:");
		lblcb2.setText("Vælg Betalingsmetode:");
		
		buttonOrderLine.setText("Tilføj ordrelinie");
		buttonOrderLine.setOnAction(event -> this.orderLineAction());
		buttonFinish.setText("Registrer Betaling");
		buttonFinish.setOnAction(event -> this.finishSale());
				
		this.add(lbllvw, 0, 0);
		this.add(lvwOrderLines, 0, 1);
		this.add(lbllvw2, 1, 0);
		this.add(lvwPrices, 1, 1);
		
		this.add(lblcb1, 0, 2);
		this.add(cbbPriceLists, 0, 3);
		
		cbbPriceLists.setItems(FXCollections.observableArrayList(Controller.getPriceLists()));
		cbbPriceLists.setPrefWidth(200);
		this.add(lblcb2, 0, 4);
		this.add(cbbPaymentMethod, 0, 5);
		cbbPaymentMethod.setItems(FXCollections.observableArrayList(Controller.paymentMethodsToArray()));
		cbbPaymentMethod.setPrefWidth(200);
		this.add(buttonOrderLine, 1, 2);
		this.add(buttonFinish, 1, 5);
	
		ChangeListener<PriceList> priceListListener = (ov, oldPriceList, newPriceList) -> this.selectedPriceListChanged();
		this.cbbPriceLists.getSelectionModel().selectedItemProperty().addListener(priceListListener);
		cbbPriceLists.getSelectionModel().select(0);	
	}
	

	
	private void finishSale() {
			if(cbbPaymentMethod.getSelectionModel().getSelectedItem() == null) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Fejlbesked");
				alert.setHeaderText("Ugyldigt input");
				alert.setContentText("laflwalflwaf");
				alert.showAndWait();
			}
			else if (lvwOrderLines.getItems().isEmpty()) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Fejlbesked");
				alert.setHeaderText("Ugyldigt input");
				alert.setContentText("awddwadwawaddwa");
				alert.showAndWait();
			}
			else {
				PriceList priceList = (PriceList) cbbPriceLists.getSelectionModel().getSelectedItem();
				ObservableList<OrderLine> orderLines = lvwOrderLines.getItems();
				PaymentMethod paymentMethod = (PaymentMethod) cbbPaymentMethod.getSelectionModel().getSelectedItem();
				ArrayList<OrderLine> orderLinesArrayList = new ArrayList(orderLines);
				Sale sale = Controller.createNormalSale(priceList, paymentMethod, orderLinesArrayList);
				System.out.println(sale);
				
				updateControls();
			}
	}



	public void updatePrices() {
		if(cbbPriceLists.getSelectionModel().getSelectedItem() != null 	&& lvwOrderLines.getItems().size() > 0) {
			PriceList priceList = (PriceList) cbbPriceLists.getSelectionModel().getSelectedItem();	
			lvwPrices.getItems().clear();
				for (OrderLine od : lvwOrderLines.getItems()) {
					lvwPrices.getItems().add(Controller.calculatePriceFromOrderLine(od, priceList));
				}
		}
	}
	
	
	public void selectedPriceListChanged() {
		PriceList priceList = (PriceList) cbbPriceLists.getSelectionModel().getSelectedItem();
		ArrayList<OrderLine> orderLines = new ArrayList(lvwOrderLines.getItems());
		if (GuiController.checkIfPriceListIsValid(priceList, orderLines)) {
			updatePrices();
			lastSelectedPriceList = (PriceList) cbbPriceLists.getSelectionModel().getSelectedItem();
		} else {
			cbbPriceLists.getSelectionModel().select(lastSelectedPriceList);
			GuiController.invalidInputAlert("Den valgte prisliste indeholder ikke nogen pris \\n\" + \"for et eller flere af de valgte ordrelinjers produkter");
		}
	}
	
	public void orderLineAction()  {
		PriceList priceList = (PriceList) cbbPriceLists.getSelectionModel().getSelectedItem();
		
		if(priceList !=  null) {
			orderLineWindow.updateControls();
			orderLineWindow.setPriceList(priceList);
			orderLineWindow.showAndWait();
		} else {
			GuiController.noValueSelectedAlert("Der skal vælges en prisliste");
		}
		
		if(orderLineWindow.getOrderLine() !=null && !lvwOrderLines.getItems().contains(orderLineWindow.getOrderLine()))
		lvwOrderLines.getItems().add(orderLineWindow.getOrderLine());
		orderLineWindow.clearWindow();
		updatePrices();
	}



	public void updateControls() {
		cbbPaymentMethod.getSelectionModel().clearSelection();
		lvwOrderLines.getItems().clear();
		lvwPrices.getItems().clear();
		cbbPriceLists.getItems().setAll(Controller.getPriceLists());
	}
}
