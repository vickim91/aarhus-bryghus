package guifx;

import controller.GuiController;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class CreateGiftBoxWindow extends Stage {
	private TextField txfAmount = new TextField();
	private Button btnFinish = new Button("Bekræft");
	private int numberOfBeers;

	
	public CreateGiftBoxWindow(String title) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane, 150, 150);
		this.setScene(scene);
	}
	
	private void initContent(GridPane pane) {
		Label lblAmount = new Label("Angiv antal øl i gaveæske");
		pane.add(lblAmount, 0, 0);
		pane.add(txfAmount, 0, 1);
		pane.add(btnFinish, 0, 2);
		btnFinish .setOnAction(event -> addAction());
	}

	public void addAction() {
		if(GuiController.checkIfNumbers(txfAmount.getText())) {
			numberOfBeers = Integer.parseInt(txfAmount.getText());
		}
		this.close();
	}
	
	public int getNumberOfBeers() {
		return this.numberOfBeers;
	}
	
}