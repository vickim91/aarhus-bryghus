package guifx;

import java.util.ArrayList;
import controller.Controller;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.GiftBox;
import model.PriceList;
import model.Product;
import model.ProductGroup;

public class GiftBoxWindow extends Stage {
	
	
	private ListView<HBox> lvwProducts = new ListView<HBox>();
	private ListView<HBox> lvwChosenProducts = new ListView<HBox>();
	private ArrayList<Product> products = new ArrayList();
	private ArrayList<Product> productsChosen = new ArrayList();
	private int numberOfBeers;
	
	private VBox vbMenu = new VBox();
	private HBox hbConfirm = new HBox();
	private Button confirm = new Button("Gennemfør");
	private Label priceTotal = new Label("Pris: ");
	private Label sum = new Label();	
	
	public GiftBoxWindow(String title) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(true);

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane, 800, 600);
		this.setScene(scene);
	}

	public void initContent(GridPane pane) {
		
		vbMenu.setPrefWidth(60);
		vbMenu.setAlignment(Pos.CENTER);
		vbMenu.setSpacing(15);
		hbConfirm.setAlignment(Pos.CENTER);
		hbConfirm.setSpacing(25);
		hbConfirm.getChildren().addAll(confirm, priceTotal, sum);
		priceTotal.setStyle("-fx-font-size: 18");
		sum.setStyle("-fx-font-size: 18");
		confirm.setStyle("-fx-background-color: #6495ed; -fx-font-size: 2em");
		confirm.setPrefSize(150, 100);
		confirm.setOnAction(e -> confirmAction());
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);	
		pane.add(vbMenu, 0, 0);
		pane.add(lvwProducts, 1, 0);
		lvwProducts.setPrefWidth(310);
		pane.add(lvwChosenProducts,2,0);
		lvwChosenProducts.setPrefWidth(280);
		pane.add(hbConfirm, 0, 1, 3, 1);
	}
	
	public void confirmAction() {
		if(lvwChosenProducts.getItems().size() < numberOfBeers) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Fejlbesked");
			alert.setHeaderText("Ikke nok øl valgt");
			alert.setContentText("Vælg flere øl for at afslutte");
			alert.showAndWait();
		}
		else {
		this.close();
		}
	}
	
	public void updateControls(GiftBox giftBox) {
		lvwChosenProducts.getItems().clear();
		lvwProducts.setDisable(false);
		productsChosen.clear();
		
		numberOfBeers = giftBox.getNumberOfBeers();
		
		for (Product product : Controller.getProductGroups().get(0).getProducts()) {
			TextField txfProduct = new TextField("produkt");
			
			txfProduct.setText(product.toString());

			HBox hb = new HBox();
			hb.setSpacing(15);
			
			Button buttonAdd = new Button("+");
			Button buttonSub = new Button("-");
			buttonAdd.setPrefSize(25, 25);
			buttonSub.setPrefSize(25, 25);
			
			buttonAdd.setOnAction(event -> addAction(product));
			buttonSub.setOnAction(event -> subtractAction(product));
			hb.getChildren().addAll(txfProduct,buttonSub,buttonAdd );
			lvwProducts.getItems().add(hb);
		}
	}

	private void subtractAction(Product product) {
		
	}

	private void addAction(Product product) {
		
		productsChosen.add(product);
		updateChosenProducts();
	}

	private void removeAction(Product product) {
		lvwChosenProducts.getItems().remove(product);
		productsChosen.remove(product);
		updateChosenProducts();
	}
	
	private void updateChosenProducts() {
		lvwChosenProducts.getItems().clear();
		
		for(Product p : productsChosen) {
		TextField txfOrderLine = new TextField();
		txfOrderLine.setText(p.toString());
		Button cancel = new Button("X");
		cancel.setOnAction(e -> removeAction(p));
		HBox hb1 = new HBox();
		hb1.setSpacing(10);

		hb1.getChildren().addAll(txfOrderLine, cancel);
		lvwChosenProducts.getItems().add(hb1);
		}
		
		if(productsChosen.size() == numberOfBeers) {
			lvwProducts.setDisable(true);
		}
		else if(productsChosen.size() < numberOfBeers) {
			lvwProducts.setDisable(false);
		}
	}
	
	public ArrayList<Product> getProductsChosen() {
		return productsChosen;
	}
}
