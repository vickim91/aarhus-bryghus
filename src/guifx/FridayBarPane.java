package guifx;

import java.util.ArrayList;
import java.util.Iterator;
import controller.Controller;
import controller.GuiController;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import model.OrderLine;
import model.PriceList;
import model.Product;
import model.ProductGroup;
import guifx.FridayBarWindow;

public class FridayBarPane extends GridPane{
	
	private ArrayList<Button> productGroupButtons = new ArrayList<>();
	
	private ListView<HBox> lvwProducts = new ListView<HBox>();
	private ListView<HBox> lvwOrderLine = new ListView<HBox>();
	private ArrayList<OrderLine> orderLines = new ArrayList();
	
	private VBox vbMenu = new VBox();
	private HBox hbConfirm = new HBox();
	private Button confirm = new Button("Gennemfør");
	private Label priceTotal = new Label("Pris: ");
	private Label sum = new Label();
	
	private double tmp = 0;
	private String s = null;
	
	private FridayBarWindow confirmWindow = new FridayBarWindow("Bekræft");
	
	public FridayBarPane() {
				
		vbMenu.setPrefWidth(60);
		vbMenu.setAlignment(Pos.CENTER);
		vbMenu.setSpacing(15);
		hbConfirm.setAlignment(Pos.CENTER);
		hbConfirm.setSpacing(25);
		hbConfirm.getChildren().addAll(confirm, priceTotal, sum);
		priceTotal.setStyle("-fx-font-size: 18");
		sum.setStyle("-fx-font-size: 18");
		confirm.setStyle("-fx-background-color: #6495ed; -fx-font-size: 2em");
		confirm.setPrefSize(150, 100);
		confirm.setOnAction(e -> confirmAction());
		this.setPadding(new Insets(10));
		this.setHgap(10);
		this.setVgap(10);
		this.setGridLinesVisible(false);	
		this.add(vbMenu, 0, 0);
		this.add(lvwProducts, 1, 0);
		lvwProducts.setPrefWidth(310);
		this.add(lvwOrderLine,2,0);
		lvwOrderLine.setPrefWidth(280);
		this.add(hbConfirm, 0, 1, 3, 1);
	}
	
	public void updateControls() {
		if(!orderLines.isEmpty() || !lvwProducts.getItems().isEmpty()) {
			orderLines.clear();
			generateOrderLineList();
			lvwProducts.getItems().clear();
		}

		PriceList priceList = Controller.getPriceLists().get(0);
		
		ArrayList<ProductGroup> matchedList = GuiController.matchProductGroupWithPriceList(priceList);
			
		productGroupButtons.clear();
		vbMenu.getChildren().clear();
			for (int i = 0; i < matchedList.size(); i++) {
				
				int index = i;
			
				productGroupButtons.add(new Button(matchedList.get(i).toString()));
			
				vbMenu.getChildren().addAll(productGroupButtons.get(i));
				productGroupButtons.get(i).setOnAction(event -> selectProductGroupAction(matchedList.get(index)));
			}		
	}

	private void selectProductGroupAction(ProductGroup productGroup) {
		generateProductList(productGroup);
	}
	
	public void generateProductList(ProductGroup productGroup) {
	
		lvwProducts.getItems().clear();
		
		for (Product product : productGroup.getProducts()) {
			TextField txfProduct = new TextField("produkt");
			
			txfProduct.setText(product.toString());
			TextField txfAmount = new TextField("1");
			txfAmount.setMaxWidth(35);
			HBox hb = new HBox();
			hb.setSpacing(15);
			
			Button buttonAdd = new Button("+");
			Button buttonSub = new Button("-");
			buttonAdd.setPrefSize(25, 25);
			buttonSub.setPrefSize(25, 25);
			
			buttonAdd.setOnAction(event -> addAction(txfAmount, product));
			buttonSub.setOnAction(event -> subtractAction(txfAmount,product));
			hb.getChildren().addAll(txfProduct,buttonSub,txfAmount,buttonAdd );
			lvwProducts.getItems().add(hb);
		}
	}
	
	public void generateOrderLineList() {
		
		lvwOrderLine.getItems().clear();
		
		for (OrderLine orderLine : orderLines) {
			TextField txfOrderLine = new TextField();
			txfOrderLine.setText(orderLine.toString());
			TextField txfPrice = new TextField();
			Button cancel = new Button("X");
			cancel.setOnAction(e -> removeAction(orderLine));
			s = String.valueOf(Controller.calculatePriceFromOrderLine(orderLine, Controller.getPriceLists().get(0)));
			txfPrice.setText(s);
			txfPrice.setMaxWidth(50);
			HBox hb1 = new HBox();
			hb1.setSpacing(10);

			hb1.getChildren().addAll(txfOrderLine, txfPrice, cancel);
			lvwOrderLine.getItems().add(hb1);
		}
		sum.setText(Double.toString(orderLineTotal()));
	}
	
	public void addAction(TextField txfAmount, Product product) {
		int amount = Integer.parseInt(txfAmount.getText());
		txfAmount.setText(Integer.toString( Integer.parseInt(txfAmount.getText().trim())));
		boolean foundMatchingProduct=false;
		for (OrderLine ol : orderLines) {
			if(ol.getProduct().equals(product)) {
				ol.setAmount(ol.getAmount()+amount);
				foundMatchingProduct = true;
			}
		}
		
		if(!foundMatchingProduct)
			orderLines.add(Controller.createOrderLine(product,amount , null, 0d));

		generateOrderLineList();
	}
	
	public void subtractAction(TextField txfAmount, Product product) {
		int amount =Integer.parseInt(txfAmount.getText());
		Iterator<OrderLine> iterator = orderLines.listIterator();
		
		while (iterator.hasNext()) {
			OrderLine next = iterator.next();
			if(next.getProduct().equals(product)) {
				next.setAmount(next.getAmount()-amount);
				if(next.getAmount() <= 0) {
					iterator.remove();
				}
			}
		}
		generateOrderLineList();
	}
	
	public void removeAction(OrderLine orderLine) {
		orderLines.remove(orderLine);
		generateOrderLineList();
	}
	
	public void confirmAction() {
		if(orderLines.isEmpty()) {
			GuiController.noValueSelectedAlert("Ingen produkt valgt");
		}
		else {
		confirmWindow.setOrderLines(new ArrayList<OrderLine>(orderLines)); 		//sender kopi af orderlines ind i FridayBarWindow	
		confirmWindow.showAndWait();
		orderLines.clear();
		generateOrderLineList();
		lvwProducts.getItems().clear();
		}
		
	}
	
	public void setPaymentMethod() {
		
	}
	
	public double orderLineTotal() {

		double tmp = 0;
		
		for (OrderLine orderLine : orderLines) {
			tmp += Controller.calculatePriceFromOrderLine(orderLine, Controller.getPriceLists().get(0));
		}
		
		return tmp;
	}
	
	public String getPrice() {
		return sum.getText();
	}
	
	
}


	
