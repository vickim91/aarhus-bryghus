package guifx;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import controller.Controller;
import controller.GuiController;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.OrderLine;
import model.Product;
import model.ProductGroup;
import model.TourSale;

public class TourSalePane extends GridPane{

	private DatePicker datepicker = new DatePicker();
	private TextField txfNumberOfPersons = new TextField();
	private ComboBox ccbHour = new ComboBox();	
	private ComboBox ccbMinutes = new ComboBox();
	private String[] hours = { "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18"};
	private String[] minutes = { "00", "15", "30", "45"};
	private TextField txfSpecialPrice = new TextField();
	private TextField txfDiscount = new TextField();
	
	public TourSalePane() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);
		
		Label lblNumberOfPersons = new Label("Antal personer");
		this.add(lblNumberOfPersons, 0, 0);
		this.add(txfNumberOfPersons, 0, 1);
		
		this.add(txfSpecialPrice, 1, 0);
		txfSpecialPrice.setPromptText("særlig pris");
		this.add(txfDiscount, 1, 1);
		txfDiscount.setPromptText("procent rabat");
		
		
		Label lblDate = new Label("Vælg dato");
		this.add(lblDate, 0, 2);
		this.add(datepicker, 0, 3);
		
		Label lblTime = new Label("Vælg tidspunkt");
		this.add(lblTime, 0, 4);
		this.add(ccbHour, 0, 5);
		ccbHour.getItems().setAll(hours);
		this.add(ccbMinutes, 1, 5);
		ccbMinutes.getItems().setAll(minutes);
		
		Button btnCreate = new Button("Opret rundvisning");
		this.add(btnCreate, 0, 6);
		btnCreate.setOnAction(event -> this.createAction());	
	}


	public void createAction() {
		ProductGroup productGroup = Controller.getProductGroupByName("Rundvisninger");
		Product product = Controller.getProductByName("Rundvisningstur", productGroup);
		
		String s = txfNumberOfPersons.getText().trim();
		Double specialPrice = null;
		Double percentageDiscount =0d;
		LocalDate date = null;
		LocalTime time = null;
		LocalDateTime dateAndTime = null;
		int hour =0;
		int minute=0;
		int amount=0;
		if(GuiController.checkIfNumbers(s) && !s.isEmpty())	{
			amount = Integer.parseInt(s);
		} else {
			GuiController.invalidInputAlert("antal deltagere skal være et tal");
		}
		
		if(datepicker.getValue() !=null) {
			date = datepicker.getValue();
		} else {
			GuiController.noValueSelectedAlert("der skal angives en dato");
		}
		
		if(ccbHour.getSelectionModel().getSelectedItem() == null || ccbMinutes.getSelectionModel().getSelectedItem() == null) {
			GuiController.noValueSelectedAlert("Der skal angives et tidspunkt");
		} else {
			hour = Integer.parseInt((String) ccbHour.getSelectionModel().getSelectedItem());
			minute = Integer.parseInt((String) ccbMinutes.getSelectionModel().getSelectedItem());
		}
		
		if(txfSpecialPrice.getText().trim().length()>0)	{
			if(GuiController.checkIfNumbers(txfSpecialPrice.getText().trim()))
				specialPrice =  Double.parseDouble(txfSpecialPrice.getText().trim());
			else {
				GuiController.invalidInputAlert("særlig pris skal være et tal");
			}
		}

	
		if(txfDiscount.getText().trim().length()>0)
			if(GuiController.checkIfNumbers(txfDiscount.getText().trim()))
				percentageDiscount =  Double.parseDouble(txfDiscount.getText().trim()); 
			else {
				GuiController.invalidInputAlert("rabat skal være et tal");
			}
				
		time = LocalTime.of(hour, minute);
		
		if(date != null && time !=null) {
			dateAndTime = LocalDateTime.of(date, time);
		
		OrderLine tourOrderLine = Controller.createOrderLine(product, amount, specialPrice, percentageDiscount);
		TourSale ts = Controller.createTourSale(dateAndTime, tourOrderLine);
		updateControls();
		}		
	}

	public void updateControls() {
		txfNumberOfPersons.clear();
		ccbHour.getSelectionModel().clearSelection();	
		ccbMinutes.getSelectionModel().clearSelection();
		txfSpecialPrice.clear();
		txfDiscount.clear();
		datepicker.setValue(null);
	}
}