package guifx;

import java.util.ArrayList;
import controller.Controller;
import controller.GuiController;
import javafx.collections.FXCollections;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.OrderLine;
import model.PaymentMethod;
import model.TapBeerRental;

public class TapBeerRentalEndSaleWindow extends Stage {
	
	private ListView<HBox> lvwItemsOut = new ListView<HBox>();
	private ArrayList<OrderLine> itemsIn = new ArrayList();
	private ArrayList<OrderLine> itemsOut;
	private ListView<HBox> lvwItemsIn = new ListView<HBox>();
	private TapBeerRental tapBeerRental;
	private Label lblItemsOut = new Label("varer ud");
	private Label lblItemsIn = new Label("varer der skal refunderes");
	private TextField txfTotalPrice = new TextField();
	private TextField txfDeposit = new TextField();
	private TextField txfDifference = new TextField();
	private TextField txfDeliveryFee = new TextField();
	private Label lblDeposit = new Label("Allerede betalt depositum");
	private Label lblTotalPrice = new Label("total pris");
	private Label lblDifference = new Label("difference til afregning");
	private Label lblDeliveryFee = new Label ("levering");
	private Label lblPaymentMethodLabel = new Label("betalingsmetode");
	private Button buttonCloseSale = new Button();
	private ComboBox cbbPaymentMethod = new ComboBox();
	
	
	public TapBeerRentalEndSaleWindow(String title, TapBeerRental tapBeerRental) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(true);
		this.setTitle(title);
		GridPane pane = new GridPane();
	
		Scene scene = new Scene(pane, 800, 400);
		this.setScene(scene);
		this.tapBeerRental = tapBeerRental;

		initContent(pane);
	}
	
	public void initContent(GridPane pane) {
		pane.add(lblItemsOut, 0, 0);
		pane.add(lvwItemsOut, 0, 1);
		lvwItemsOut.setPrefWidth(600);
		pane.add(lblItemsIn, 1, 0);
		pane.add(lvwItemsIn, 1, 1);
		lvwItemsIn.setPrefWidth(600);
		
		pane.add(lblDeposit, 0, 2);
		pane.add(txfDeposit, 0, 3);
		pane.add(lblDeliveryFee, 0, 4);
		pane.add(txfDeliveryFee, 0, 5);
		pane.add(lblDifference, 0, 8);
		pane.add(txfDifference, 0, 9);
		pane.add(lblTotalPrice, 0, 6);
		pane.add(txfTotalPrice, 0, 7);
		
		pane.add(lblPaymentMethodLabel, 1, 2);
		pane.add(cbbPaymentMethod, 1, 3);
		cbbPaymentMethod.setItems(FXCollections.observableArrayList(Controller.paymentMethodsToArray()));

		cbbPaymentMethod.setPrefWidth(200);
		cbbPaymentMethod.getItems().remove(PaymentMethod.BEERCARD);
		
		pane.add(buttonCloseSale, 1, 4);
		buttonCloseSale.setText("afslut salg");
		buttonCloseSale.setOnAction(event -> completeeSaleAction());
			
		txfTotalPrice.setEditable(false);
		txfDeposit.setText(String.valueOf(tapBeerRental.getDeposit()));
		txfDeposit.setEditable(false);
		txfDeliveryFee.setText(String.valueOf(tapBeerRental.getActualDeliveryFee()));
		txfDeposit.setEditable(false);
		itemsOut = new ArrayList();
		
		for(OrderLine ol : tapBeerRental.getItemsOut()) {
			OrderLine olTemp =  new OrderLine(ol.getProduct(), ol.getAmount(), ol.getSpecialPrice(), ol.getPercentageDiscount());
			itemsOut.add(olTemp);
		}
		updateAction();
	}
	
	public void generateOrderLineList() {

		lvwItemsOut.getItems().clear();
		
		for (OrderLine orderLine : itemsOut) {
			TextField txfOrderLine = new TextField();
			txfOrderLine.setText(orderLine.getProduct().toString());
			TextField txfAmount = new TextField();
			txfAmount.setText(String.valueOf(orderLine.getAmount()));
			Button moveButton = new Button("fjern 1 ubrugt vare fra salg");
			if(orderLine.getProduct().getGroup().isRental()) {
				moveButton.setDisable(true);
			}

			txfAmount.setMaxWidth(50);
			HBox hb1 = new HBox();
			hb1.setSpacing(10);
			moveButton.setOnAction(e -> moveAction(itemsOut,itemsIn,orderLine, txfAmount));
			hb1.getChildren().addAll(txfOrderLine, txfAmount, moveButton);
			lvwItemsOut.getItems().add(hb1);
		}

	}
	
	public void generateRefundableItemsList() {
		lvwItemsIn.getItems().clear();
	
		for (OrderLine orderLine : itemsIn) {
			TextField txfOrderLine = new TextField();
			txfOrderLine.setText(orderLine.getProduct().toString());
			TextField txfAmount = new TextField();
			txfAmount.setText(String.valueOf(orderLine.getAmount()));
			Button undoButton = new Button ("fortryd");
			HBox hb1 = new HBox();
			hb1.setSpacing(10);
		
			undoButton.setOnAction(event -> moveAction(itemsIn,itemsOut,orderLine, txfAmount));
			hb1.getChildren().addAll(txfOrderLine, txfAmount, undoButton);
			lvwItemsIn.getItems().add(hb1);
		}
	}




 public void moveAction(ArrayList<OrderLine> moveFrom, ArrayList<OrderLine> moveTo, OrderLine orderLine, TextField txfAmount) {

	if(Integer.parseInt(txfAmount.getText())>=1) {
		boolean foundMatchingProduct=false;
		boolean hasMoved =false;

		for(OrderLine ol : moveTo) {
			boolean sameName = (orderLine.getProduct().getName().equals(ol.getProduct().getName()));

			if(sameName && !hasMoved) {
				ol.setAmount(ol.getAmount()+1);
				orderLine.setAmount(orderLine.getAmount()-1);
			
				if(orderLine.getAmount()<=0) {
					moveFrom.remove(orderLine);
				}
			
				txfAmount.setText(String.valueOf(orderLine.getAmount()));
				foundMatchingProduct = true;
				hasMoved = true;
			}
		}
		
		if(!foundMatchingProduct) {

			OrderLine olTemp = new OrderLine(orderLine.getProduct(), 1, orderLine.getSpecialPrice(), orderLine.getPercentageDiscount());
			moveTo.add(olTemp);
			orderLine.setAmount(orderLine.getAmount()-1);
			txfAmount.setText(String.valueOf(orderLine.getAmount()));
		}
	
		updateAction();
	 } else {

	 	}
 	}
 
	public void updateAction() {
		generateOrderLineList();
		generateRefundableItemsList();

		Controller.calculateFinalSaleInTapBeerRental(itemsIn, tapBeerRental);
		txfDifference.setText(String.valueOf(Controller.calculateDifferenceAfterRental(tapBeerRental)));
		txfTotalPrice.setText(String.valueOf(Controller.calculateTotalPrice(tapBeerRental)));
	}

	public void completeeSaleAction() {
		PaymentMethod paymentMethod = (PaymentMethod) cbbPaymentMethod.getSelectionModel().getSelectedItem();
	
		if(paymentMethod == null) {
			GuiController.noValueSelectedAlert("Der skal angives en betalingsmetode");
		} else {
			Controller.setPaymentMethodInSale(paymentMethod, tapBeerRental);
			Controller.moveSaleFromUnpaidToCompleted(tapBeerRental);
			this.close();
		}
	}
}
